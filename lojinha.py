#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, datetime, ConfigParser, shutil, zipfile, time, subprocess

from PyQt4 import QtGui
from PyQt4 import QtCore

from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.units import mm
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics

reload(sys)
sys.setdefaultencoding("utf-8")

dirpathHome = os.environ.get('HOME', None)
dirpathConfig = os.path.join(dirpathHome, '.lojinha')
if not os.path.isdir(dirpathConfig):
    os.mkdir(dirpathConfig)

dirpathNames = os.path.join(dirpathConfig, 'irmaos')
if not os.path.isdir(os.path.join(dirpathConfig, 'irmaos')):
    os.mkdir(os.path.join(dirpathConfig, 'irmaos'))

dirpathProducts = os.path.join(dirpathConfig, 'produtos')
if not os.path.isdir(os.path.join(dirpathConfig, 'produtos')):
    os.mkdir(os.path.join(dirpathConfig, 'produtos'))

for product in os.listdir(os.path.join(dirpathConfig, 'produtos')):
    product_profile = ConfigParser.RawConfigParser()
    product_profile.read(os.path.join(dirpathConfig, 'produtos', product))
    if not product_profile.has_option('geral', 'valor-variavel'):
        product_profile.set('geral', 'valor-variavel', False)
    product_profile.write(open(os.path.join(dirpathConfig, 'produtos', product), 'wb'))


dirpathSoftware = os.path.abspath(os.path.dirname(__file__))

mainConfigFile = ConfigParser.RawConfigParser()
mainConfigFile.read(os.path.join(dirpathConfig, 'config.cfg'))



if not os.path.isfile(os.path.join(dirpathConfig, 'config.cfg')):
    mainConfigFile.add_section('geral')
    mainConfigFile.set('geral', 'titulo-principal', 'Lojinha')
    mainConfigFile.set('geral', 'subtitulo', 'Salão de Assembleias')
    mainConfigFile.set('geral', 'membro-donativo', '0')
    mainConfigFile.set('geral', 'membro-digitos', '7')
    mainConfigFile.set('geral', 'senha', 'admin')

    profile = ConfigParser.RawConfigParser()
    profile.read(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo')))))

    profile.add_section('geral')
    profile.set('geral', 'nome', 'Donativo')
    profile.set('geral', 'ultimo-balanco', datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d") + datetime.datetime.now().strftime("%H") + datetime.datetime.now().strftime("%M"))
    profile.add_section('produtos')
    profile.set('produtos', 'lista', '')
    profile.write(open(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo')))), 'wb'))


list_of_months = [u"Janeiro", u"Fevereiro", u"Março", u"Abril", u"Maio", u"Junho", u"Julho", u"Agosto", u"Setembro", u"Outubro", u"Novembro", u"Dezembro"]
list_of_members = []
list_of_products = []


class mainWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.resize(QtGui.QDesktopWidget().screenGeometry().width(), QtGui.QDesktopWidget().screenGeometry().height())
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.FramelessWindowHint)
        self.setWindowIcon(QtGui.QIcon('/usr/share/pixmaps/lojinha.png'))
        self.setCursor(QtGui.QCursor(10))

        self.parcial_price = 0.0
        self.voice = False

        self.main_panel = QtGui.QWidget(parent=self)
        self.main_panel.setGeometry(0, 0, QtGui.QDesktopWidget().screenGeometry().width(), QtGui.QDesktopWidget().screenGeometry().height())

        self.main_panel_background = QtGui.QLabel(parent=self.main_panel)
        self.main_panel_background.setGeometry(0, 0, QtGui.QDesktopWidget().screenGeometry().width(), QtGui.QDesktopWidget().screenGeometry().height())
        self.main_panel_background.setPixmap(QtGui.QPixmap(os.path.join(dirpathSoftware, 'fundo.png')))
        self.main_panel_background.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignCenter)

        self.main_product_details_panel = QtGui.QWidget(parent=self.main_panel)
        self.main_product_details_panel.setGeometry(QtGui.QDesktopWidget().screenGeometry().width() * 0.36, 0, int(QtGui.QDesktopWidget().screenGeometry().width() * 0.65) - int(QtGui.QDesktopWidget().screenGeometry().width() * 0.04), QtGui.QDesktopWidget().screenGeometry().height())

        self.main_product_details_panel_transparency = QtGui.QGraphicsOpacityEffect()
        self.main_product_details_panel.setGraphicsEffect(self.main_product_details_panel_transparency)
        self.main_product_details_panel_transparency.setOpacity(0.0)

        self.main_product_details_panel_animation = QtCore.QPropertyAnimation(self.main_product_details_panel_transparency, 'opacity')
        self.main_product_details_panel_animation.setEasingCurve(QtCore.QEasingCurve(26))

        self.main_product_details_panel_title = QtGui.QLabel(parent=self.main_product_details_panel)
        self.main_product_details_panel_title.setWordWrap(True)
        self.main_product_details_panel_title.setFont(QtGui.QFont('Ubuntu', 20))
        self.main_product_details_panel_title.setGeometry(0, int(self.main_product_details_panel.height() * 0.15), self.main_product_details_panel.width(), int(self.main_product_details_panel.height() * 0.3))
        self.main_product_details_panel_title.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)

        self.main_product_details_panel_units_box = QtGui.QWidget(parent=self.main_product_details_panel)
        self.main_product_details_panel_units_box.setObjectName('main_product_details_panel_units_box')
        self.main_product_details_panel_units_box.setGeometry(0, int(self.main_product_details_panel.height()) * 0.45, int(self.main_product_details_panel.width() * 0.4), 150)
        self.main_product_details_panel_units_box.setStyleSheet("QWidget#main_product_details_panel_units_box { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")

        self.main_product_details_panel_units_box_number = QtGui.QLabel(parent=self.main_product_details_panel_units_box)
        self.main_product_details_panel_units_box_number.setFont(QtGui.QFont('Ubuntu', 60))
        self.main_product_details_panel_units_box_number.setGeometry(0, 0, self.main_product_details_panel_units_box.width() * 0.4, 110)
        self.main_product_details_panel_units_box_number.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignCenter)

        self.main_product_details_panel_units_box_unit = QtGui.QLabel(parent=self.main_product_details_panel_units_box)
        self.main_product_details_panel_units_box_unit.setFont(QtGui.QFont('Ubuntu', 18, 75))
        self.main_product_details_panel_units_box_unit.setGeometry(self.main_product_details_panel_units_box.width() * 0.4, 0, self.main_product_details_panel_units_box.width() * 0.6, 110)
        self.main_product_details_panel_units_box_unit.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignCenter)

        self.main_product_details_panel_units_box_tip = QtGui.QLabel(parent=self.main_product_details_panel_units_box)
        self.main_product_details_panel_units_box_tip.setWordWrap(True)
        self.main_product_details_panel_units_box_tip.setText('USE + OU - NO TECLADO')
        self.main_product_details_panel_units_box_tip.setFont(QtGui.QFont('Ubuntu', 10))
        self.main_product_details_panel_units_box_tip.setGeometry(self.main_product_details_panel_units_box.width() * 0.1, 110, self.main_product_details_panel_units_box.width() * 0.8, 40)
        self.main_product_details_panel_units_box_tip.setForegroundRole(QtGui.QPalette.ColorRole(5))
        self.main_product_details_panel_units_box_tip.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignCenter)

        self.main_product_details_panel_price = QtGui.QLabel(parent=self.main_product_details_panel)
        self.main_product_details_panel_price.setFont(QtGui.QFont('Ubuntu', 50))
        self.main_product_details_panel_price.setGeometry(self.main_product_details_panel.width() * 0.4, self.main_product_details_panel.height() * 0.45, self.main_product_details_panel.width() * 0.6, 150)
        self.main_product_details_panel_price.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignCenter)

        self.main_product_details_panel_price_field = QtGui.QLineEdit(parent=self.main_product_details_panel)
        self.main_product_details_panel_price_field.setFont(QtGui.QFont('Ubuntu', 50))
        self.main_product_details_panel_price_field.setText('R$ 0,00')
        self.main_product_details_panel_price_field.setGeometry(0, int(self.main_product_details_panel.height()) * 0.45, self.main_product_details_panel.width(), 150)
        self.main_product_details_panel_price_field.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignCenter)
        self.main_product_details_panel_price_field.setStyleSheet("QWidget#main_product_details_panel_units_box { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.main_product_details_panel_price_field.textChanged.connect(self.main_product_details_panel_price_field_changed)

        self.main_product_details_panel_add_button = QtGui.QWidget(parent=self.main_product_details_panel)
        self.main_product_details_panel_add_button.setObjectName('main_product_details_panel_add_button')
        self.main_product_details_panel_add_button.setGeometry(0, int(self.main_product_details_panel.height() - int(self.main_product_details_panel.height() * 0.2)), int(self.main_product_details_panel.width() * 0.49), int(self.main_product_details_panel.height()) * 0.15)
        self.main_product_details_panel_add_button.setStyleSheet("QWidget#main_product_details_panel_add_button { border : none ; background: rgba(255,126,0,20%) ; border-radius: 8px;}")

        self.main_product_details_panel_add_button_label = QtGui.QLabel(parent=self.main_product_details_panel_add_button)
        self.main_product_details_panel_add_button_label.setFont(QtGui.QFont('Ubuntu', 22, 75))
        self.main_product_details_panel_add_button_label.setText(u'Sim')
        self.main_product_details_panel_add_button_label.setGeometry(0, 0, self.main_product_details_panel_add_button.width(), self.main_product_details_panel_add_button.height())
        self.main_product_details_panel_add_button_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignCenter)

        self.main_product_details_panel_cancel_button = QtGui.QWidget(parent=self.main_product_details_panel)
        self.main_product_details_panel_cancel_button.setObjectName('main_product_details_panel_cancel_button')
        self.main_product_details_panel_cancel_button.setGeometry(int(self.main_product_details_panel.width() * 0.51), int(self.main_product_details_panel.height() - int(self.main_product_details_panel.height() * 0.2)), int(self.main_product_details_panel.width() * 0.49), int(self.main_product_details_panel.height()) * 0.15)
        self.main_product_details_panel_cancel_button.setStyleSheet("QWidget#main_product_details_panel_cancel_button { border : none ; background: rgba(255,126,0,20%) ; border-radius: 8px;}")

        self.main_product_details_panel_cancel_button_label = QtGui.QLabel(parent=self.main_product_details_panel_cancel_button)
        self.main_product_details_panel_cancel_button_label.setFont(QtGui.QFont('Ubuntu', 22, 75))
        self.main_product_details_panel_cancel_button_label.setText(u'Não')
        self.main_product_details_panel_cancel_button_label.setGeometry(0, 0, self.main_product_details_panel_cancel_button.width(), self.main_product_details_panel_cancel_button.height())
        self.main_product_details_panel_cancel_button_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignCenter)

        self.main_product_code_panel = QtGui.QWidget(parent=self.main_panel)
        self.main_product_code_panel.setGeometry(QtGui.QDesktopWidget().screenGeometry().width(), int(QtGui.QDesktopWidget().screenGeometry().height() / 2) - 120, int(QtGui.QDesktopWidget().screenGeometry().width() * 0.66) -120, 240)

        self.main_product_code_panel_transparency = QtGui.QGraphicsOpacityEffect()
        self.main_product_code_panel.setGraphicsEffect(self.main_product_code_panel_transparency)
        self.main_product_code_panel_transparency.setOpacity(0.0)

        self.main_product_code_panel_animation = QtCore.QPropertyAnimation(self.main_product_code_panel, 'geometry')
        self.main_product_code_panel_animation.setEasingCurve(QtCore.QEasingCurve(26))

        self.main_product_code_panel_animation_transparency = QtCore.QPropertyAnimation(self.main_product_code_panel_transparency, 'opacity')
        self.main_product_code_panel_animation_transparency.setEasingCurve(QtCore.QEasingCurve(26))

        self.main_product_code_title = QtGui.QLabel(parent=self.main_product_code_panel)
        self.main_product_code_title.setFont(QtGui.QFont('Ubuntu', 20))
        self.main_product_code_title.setGeometry(0, 0, self.main_product_code_panel.width(), 100)
        self.main_product_code_title.setAlignment(QtCore.Qt.AlignLeft)

        self.main_product_code_number = QtGui.QLineEdit(parent=self.main_product_code_panel)
        self.main_product_code_number.setGeometry(0,100,self.main_product_code_panel.width(), 100)
        self.main_product_code_number.setFont(QtGui.QFont('Ubuntu', 40))
        self.main_product_code_number.setAlignment(QtCore.Qt.AlignLeft)
        self.main_product_code_number.textChanged.connect(self.change_product_number)
        self.main_product_code_number.editingFinished.connect(self.change_product_number_finished)
        self.main_product_code_number.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,20%) ; border-radius: 8px;}")
        self.main_product_code_number.setBackgroundRole(QtGui.QPalette.ColorRole(0))

        self.main_product_code_number_exit_tip = QtGui.QLabel(parent=self.main_product_code_panel)
        self.main_product_code_number_exit_tip.setWordWrap(True)
        self.main_product_code_number_exit_tip.setText(u'SE NÃO TEM MAIS ITENS PARA ADICIONAR, TECLE <strong>NÃO</strong>')
        self.main_product_code_number_exit_tip.setFont(QtGui.QFont('Ubuntu', 10))
        self.main_product_code_number_exit_tip.setGeometry(0,200,self.main_product_code_panel.width(), 40)
        self.main_product_code_number_exit_tip.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.main_panel_donate_tip = QtGui.QLabel(parent=self.main_panel)
        self.main_panel_donate_tip.setWordWrap(True)
        self.main_panel_donate_tip.setShown(True)
        self.main_panel_donate_tip.setFont(QtGui.QFont('Ubuntu', 28, 75))
        self.main_panel_donate_tip.setGeometry(int(self.main_panel.width() * 0.1), int(self.main_panel.height() * 0.3), int(self.main_panel.width() * 0.8), int(self.main_panel.height() * 0.6))
        self.main_panel_donate_tip.setAlignment(QtCore.Qt.AlignCenter)

        self.left_panel = QtGui.QWidget(parent=self.main_panel)
        self.left_panel.setGeometry(0, 0, int(QtGui.QDesktopWidget().screenGeometry().width() * 0.33), QtGui.QDesktopWidget().screenGeometry().height())

        self.left_panel_background = QtGui.QLabel(parent=self.left_panel)
        self.left_panel_background.setGeometry(0, 0, self.left_panel.width(), self.left_panel.height())
        self.left_panel_background.setPixmap(QtGui.QPixmap(os.path.join(dirpathSoftware, 'painel_esquerda.png')))
        self.left_panel_background.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignCenter)

        self.left_panel_title = QtGui.QLabel(parent=self.left_panel)
        self.left_panel_title.setWordWrap(True)
        self.left_panel_title.setFont(QtGui.QFont('Ubuntu', 16, 75))
        self.left_panel_title.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.15), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.1))
        self.left_panel_title.setAlignment(QtCore.Qt.AlignLeft)

        self.left_panel_table_01 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_01.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.2), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_01_border = QtGui.QWidget(parent=self.left_panel_table_01)
        self.left_panel_table_01_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_01_border.setGeometry(0,0,self.left_panel_table_01.width(),self.left_panel_table_01.height())

        self.left_panel_table_01_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_01.setGraphicsEffect(self.left_panel_table_01_transparency)
        self.left_panel_table_01_transparency.setOpacity(1.0)

        self.left_panel_table_01_title = QtGui.QLabel(parent=self.left_panel_table_01)
        self.left_panel_table_01_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_01_title.setGeometry(self.left_panel_table_01.width() * 0.02, 0, self.left_panel_table_01.width() * 0.96, self.left_panel_table_01.height())
        self.left_panel_table_01_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_01_price = QtGui.QLabel(parent=self.left_panel_table_01)
        self.left_panel_table_01_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_01_price.setGeometry(self.left_panel_table_01.width() * 0.02, 0, self.left_panel_table_01.width() * 0.96, self.left_panel_table_01.height())
        self.left_panel_table_01_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_table_01_transparency_animation = QtCore.QPropertyAnimation(self.left_panel_table_01_transparency, 'opacity')
        self.left_panel_table_01_transparency_animation.setEasingCurve(QtCore.QEasingCurve(26))
        self.left_panel_table_01_geometry_animation = QtCore.QPropertyAnimation(self.left_panel_table_01, 'geometry')
        self.left_panel_table_01_geometry_animation.setEasingCurve(QtCore.QEasingCurve(26))

        self.left_panel_table_02 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_02.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.26), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_02_border = QtGui.QWidget(parent=self.left_panel_table_02)
        self.left_panel_table_02_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_02_border.setGeometry(0,0,self.left_panel_table_02.width(),self.left_panel_table_02.height())

        self.left_panel_table_02_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_02.setGraphicsEffect(self.left_panel_table_02_transparency)
        self.left_panel_table_02_transparency.setOpacity(0.9)

        self.left_panel_table_02_title = QtGui.QLabel(parent=self.left_panel_table_02)
        self.left_panel_table_02_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_02_title.setGeometry(self.left_panel_table_02.width() * 0.02, 0, self.left_panel_table_02.width() * 0.96, self.left_panel_table_02.height())
        self.left_panel_table_02_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_02_price = QtGui.QLabel(parent=self.left_panel_table_02)
        self.left_panel_table_02_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_02_price.setGeometry(self.left_panel_table_02.width() * 0.02, 0, self.left_panel_table_02.width() * 0.96, self.left_panel_table_02.height())
        self.left_panel_table_02_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_table_03 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_03.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.32), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_03_border = QtGui.QWidget(parent=self.left_panel_table_03)
        self.left_panel_table_03_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_03_border.setGeometry(0,0,self.left_panel_table_03.width(),self.left_panel_table_03.height())

        self.left_panel_table_03_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_03.setGraphicsEffect(self.left_panel_table_03_transparency)
        self.left_panel_table_03_transparency.setOpacity(0.8)

        self.left_panel_table_03_title = QtGui.QLabel(parent=self.left_panel_table_03)
        self.left_panel_table_03_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_03_title.setGeometry(self.left_panel_table_03.width() * 0.02, 0, self.left_panel_table_03.width() * 0.96, self.left_panel_table_03.height())
        self.left_panel_table_03_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_03_price = QtGui.QLabel(parent=self.left_panel_table_03)
        self.left_panel_table_03_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_03_price.setGeometry(self.left_panel_table_03.width() * 0.02, 0, self.left_panel_table_03.width() * 0.96, self.left_panel_table_03.height())
        self.left_panel_table_03_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_table_04 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_04.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.38), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_04_border = QtGui.QWidget(parent=self.left_panel_table_04)
        self.left_panel_table_04_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_04_border.setGeometry(0,0,self.left_panel_table_04.width(),self.left_panel_table_04.height())

        self.left_panel_table_04_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_04.setGraphicsEffect(self.left_panel_table_04_transparency)
        self.left_panel_table_04_transparency.setOpacity(0.7)

        self.left_panel_table_04_title = QtGui.QLabel(parent=self.left_panel_table_04)
        self.left_panel_table_04_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_04_title.setGeometry(self.left_panel_table_04.width() * 0.02, 0, self.left_panel_table_04.width() * 0.96, self.left_panel_table_04.height())
        self.left_panel_table_04_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_04_price = QtGui.QLabel(parent=self.left_panel_table_04)
        self.left_panel_table_04_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_04_price.setGeometry(self.left_panel_table_04.width() * 0.02, 0, self.left_panel_table_04.width() * 0.96, self.left_panel_table_04.height())
        self.left_panel_table_04_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_table_05 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_05.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.44), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_05_border = QtGui.QWidget(parent=self.left_panel_table_05)
        self.left_panel_table_05_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_05_border.setGeometry(0,0,self.left_panel_table_05.width(),self.left_panel_table_05.height())

        self.left_panel_table_05_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_05.setGraphicsEffect(self.left_panel_table_05_transparency)
        self.left_panel_table_05_transparency.setOpacity(0.6)

        self.left_panel_table_05_title = QtGui.QLabel(parent=self.left_panel_table_05)
        self.left_panel_table_05_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_05_title.setGeometry(self.left_panel_table_05.width() * 0.02, 0, self.left_panel_table_05.width() * 0.96, self.left_panel_table_05.height())
        self.left_panel_table_05_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_05_price = QtGui.QLabel(parent=self.left_panel_table_05)
        self.left_panel_table_05_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_05_price.setGeometry(self.left_panel_table_05.width() * 0.02, 0, self.left_panel_table_05.width() * 0.96, self.left_panel_table_05.height())
        self.left_panel_table_05_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_table_06 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_06.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.50), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_06_border = QtGui.QWidget(parent=self.left_panel_table_06)
        self.left_panel_table_06_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_06_border.setGeometry(0,0,self.left_panel_table_06.width(),self.left_panel_table_06.height())

        self.left_panel_table_06_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_06.setGraphicsEffect(self.left_panel_table_06_transparency)
        self.left_panel_table_06_transparency.setOpacity(0.5)

        self.left_panel_table_06_title = QtGui.QLabel(parent=self.left_panel_table_06)
        self.left_panel_table_06_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_06_title.setGeometry(self.left_panel_table_06.width() * 0.02, 0, self.left_panel_table_06.width() * 0.96, self.left_panel_table_06.height())
        self.left_panel_table_06_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_06_price = QtGui.QLabel(parent=self.left_panel_table_06)
        self.left_panel_table_06_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_06_price.setGeometry(self.left_panel_table_06.width() * 0.02, 0, self.left_panel_table_06.width() * 0.96, self.left_panel_table_06.height())
        self.left_panel_table_06_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_table_07 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_07.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.56), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_07_border = QtGui.QWidget(parent=self.left_panel_table_07)
        self.left_panel_table_07_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_07_border.setGeometry(0,0,self.left_panel_table_07.width(),self.left_panel_table_07.height())

        self.left_panel_table_07_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_07.setGraphicsEffect(self.left_panel_table_07_transparency)
        self.left_panel_table_07_transparency.setOpacity(0.4)

        self.left_panel_table_07_title = QtGui.QLabel(parent=self.left_panel_table_07)
        self.left_panel_table_07_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_07_title.setGeometry(self.left_panel_table_07.width() * 0.02, 0, self.left_panel_table_07.width() * 0.96, self.left_panel_table_07.height())
        self.left_panel_table_07_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_07_price = QtGui.QLabel(parent=self.left_panel_table_07)
        self.left_panel_table_07_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_07_price.setGeometry(self.left_panel_table_07.width() * 0.02, 0, self.left_panel_table_07.width() * 0.96, self.left_panel_table_07.height())
        self.left_panel_table_07_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_table_08 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_08.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.62), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_08_border = QtGui.QWidget(parent=self.left_panel_table_08)
        self.left_panel_table_08_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_08_border.setGeometry(0,0,self.left_panel_table_08.width(),self.left_panel_table_08.height())

        self.left_panel_table_08_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_08.setGraphicsEffect(self.left_panel_table_08_transparency)
        self.left_panel_table_08_transparency.setOpacity(0.3)

        self.left_panel_table_08_title = QtGui.QLabel(parent=self.left_panel_table_08)
        self.left_panel_table_08_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_08_title.setGeometry(self.left_panel_table_08.width() * 0.02, 0, self.left_panel_table_08.width() * 0.96, self.left_panel_table_08.height())
        self.left_panel_table_08_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_08_price = QtGui.QLabel(parent=self.left_panel_table_08)
        self.left_panel_table_08_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_08_price.setGeometry(self.left_panel_table_08.width() * 0.02, 0, self.left_panel_table_08.width() * 0.96, self.left_panel_table_08.height())
        self.left_panel_table_08_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_table_09 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_09.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.68), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_09_border = QtGui.QWidget(parent=self.left_panel_table_09)
        self.left_panel_table_09_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_09_border.setGeometry(0,0,self.left_panel_table_09.width(),self.left_panel_table_09.height())

        self.left_panel_table_09_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_09.setGraphicsEffect(self.left_panel_table_09_transparency)
        self.left_panel_table_09_transparency.setOpacity(0.2)

        self.left_panel_table_09_title = QtGui.QLabel(parent=self.left_panel_table_09)
        self.left_panel_table_09_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_09_title.setGeometry(self.left_panel_table_09.width() * 0.02, 0, self.left_panel_table_09.width() * 0.96, self.left_panel_table_09.height())
        self.left_panel_table_09_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_09_price = QtGui.QLabel(parent=self.left_panel_table_09)
        self.left_panel_table_09_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_09_price.setGeometry(self.left_panel_table_09.width() * 0.02, 0, self.left_panel_table_09.width() * 0.96, self.left_panel_table_09.height())
        self.left_panel_table_09_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_table_10 = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_table_10.setGeometry(int(self.left_panel.width() * 0.1), int(self.left_panel.height() * 0.74), int(self.left_panel.width() * 0.8), int(self.left_panel.height() * 0.05))

        self.left_panel_table_10_border = QtGui.QWidget(parent=self.left_panel_table_10)
        self.left_panel_table_10_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
        self.left_panel_table_10_border.setGeometry(0,0,self.left_panel_table_10.width(),self.left_panel_table_10.height())

        self.left_panel_table_10_transparency = QtGui.QGraphicsOpacityEffect()
        self.left_panel_table_10.setGraphicsEffect(self.left_panel_table_10_transparency)
        self.left_panel_table_10_transparency.setOpacity(0.1)

        self.left_panel_table_10_title = QtGui.QLabel(parent=self.left_panel_table_10)
        self.left_panel_table_10_title.setFont(QtGui.QFont('Ubuntu', 10))
        self.left_panel_table_10_title.setGeometry(self.left_panel_table_10.width() * 0.02, 0, self.left_panel_table_10.width() * 0.96, self.left_panel_table_10.height())
        self.left_panel_table_10_title.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_table_10_price = QtGui.QLabel(parent=self.left_panel_table_10)
        self.left_panel_table_10_price.setFont(QtGui.QFont('Ubuntu', 14))
        self.left_panel_table_10_price.setGeometry(self.left_panel_table_10.width() * 0.02, 0, self.left_panel_table_10.width() * 0.96, self.left_panel_table_10.height())
        self.left_panel_table_10_price.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_total_box = QtGui.QWidget(parent=self.left_panel)
        self.left_panel_total_box.setGeometry(0, int(self.left_panel.height() - int(self.left_panel.height() * 0.2)), self.left_panel.width(), self.left_panel.height() * 0.15)

        self.left_panel_total_box_background = QtGui.QWidget(parent=self.left_panel_total_box)
        self.left_panel_total_box_background.setGeometry(0, 0, self.left_panel_total_box.width(), self.left_panel_total_box.height())
        self.left_panel_total_box_background.setStyleSheet("QWidget { border : none ; background: rgba(20,20,20,25%);}")

        self.left_panel_total_title1 = QtGui.QLabel(parent=self.left_panel_total_box)
        self.left_panel_total_title1.setText("<strong>TOTAL</strong><br><small>AGORA</small>")
        self.left_panel_total_title1.setWordWrap(True)
        self.left_panel_total_title1.setFont(QtGui.QFont('Ubuntu', 12))
        self.left_panel_total_title1.setForegroundRole(QtGui.QPalette.ColorRole(2))
        self.left_panel_total_title1.setGeometry(int(self.left_panel.width() * 0.1), self.left_panel_total_box.height() * 0.1, int(self.left_panel_total_box.width() - int(self.left_panel.width() * 0.1)), self.left_panel_total_box.height() * 0.4)
        self.left_panel_total_title1.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_total_value1 = QtGui.QLabel(parent=self.left_panel_total_box)
        self.left_panel_total_value1.setWordWrap(True)
        self.left_panel_total_value1.setFont(QtGui.QFont('Ubuntu', 24))
        self.left_panel_total_value1.setForegroundRole(QtGui.QPalette.ColorRole(2))
        self.left_panel_total_value1.setGeometry(int(self.left_panel.width() * 0.1), self.left_panel_total_box.height() * 0.1, int(self.left_panel_total_box.width() - int(self.left_panel.width() * 0.2)), self.left_panel_total_box.height() * 0.4)
        self.left_panel_total_value1.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.left_panel_total_title2 = QtGui.QLabel(parent=self.left_panel_total_box)
        self.left_panel_total_title2.setWordWrap(True)
        self.left_panel_total_title2.setFont(QtGui.QFont('Ubuntu', 12))
        self.left_panel_total_title2.setForegroundRole(QtGui.QPalette.ColorRole(2))
        self.left_panel_total_title2.setGeometry(int(self.left_panel.width() * 0.1), self.left_panel_total_box.height() * 0.5, int(self.left_panel_total_box.width() - int(self.left_panel.width() * 0.1)), self.left_panel_total_box.height() * 0.4)
        self.left_panel_total_title2.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)

        self.left_panel_total_value2 = QtGui.QLabel(parent=self.left_panel_total_box)
        self.left_panel_total_value2.setWordWrap(True)
        self.left_panel_total_value2.setFont(QtGui.QFont('Ubuntu', 24))
        self.left_panel_total_value2.setForegroundRole(QtGui.QPalette.ColorRole(2))
        self.left_panel_total_value2.setGeometry(int(self.left_panel.width() * 0.1), self.left_panel_total_box.height() * 0.5, int(self.left_panel_total_box.width() - int(self.left_panel.width() * 0.2)), self.left_panel_total_box.height() * 0.4)
        self.left_panel_total_value2.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self.bottom_panel = QtGui.QWidget(parent=self)
        self.bottom_panel.setGeometry(0, int(QtGui.QDesktopWidget().screenGeometry().height() * 0.4), QtGui.QDesktopWidget().screenGeometry().width(), int(QtGui.QDesktopWidget().screenGeometry().height()))
        self.bottom_panel_animation = QtCore.QPropertyAnimation(self.bottom_panel, 'geometry')
        self.bottom_panel_animation.setEasingCurve(QtCore.QEasingCurve(14))

        self.bottom_panel_background = QtGui.QLabel(parent=self.bottom_panel)
        self.bottom_panel_background.setGeometry(0, 0, self.bottom_panel.width(), self.bottom_panel.height())
        self.bottom_panel_background.setPixmap(QtGui.QPixmap(os.path.join(dirpathSoftware, 'abaixo.png')))
        self.bottom_panel_background.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)

        self.bottom_number_panel = QtGui.QWidget(parent=self.bottom_panel)
        self.bottom_number_panel.setGeometry(0, 0, self.bottom_panel.width(), int(self.bottom_panel.height() * 0.6))

        self.bottom_number_panel_transparency = QtGui.QGraphicsOpacityEffect()
        self.bottom_number_panel.setGraphicsEffect(self.bottom_number_panel_transparency)
        self.bottom_number_panel_transparency.setOpacity(1.0)

        self.bottom_number_panel_transparency_animation = QtCore.QPropertyAnimation(self.bottom_number_panel_transparency, 'opacity')
        self.bottom_number_panel_transparency_animation.setEasingCurve(QtCore.QEasingCurve(26))

        self.bottom_panel_title = QtGui.QLabel(parent=self.bottom_number_panel)
        self.bottom_panel_title.setText(u"Qual é o seu número?")
        self.bottom_panel_title.setFont(QtGui.QFont('Ubuntu', 20, 75))
        self.bottom_panel_title.setGeometry(0, int(self.bottom_number_panel.height() * 0.4), self.bottom_number_panel.width(), int(self.bottom_number_panel.height() * 0.1))
        self.bottom_panel_title.setAlignment(QtCore.Qt.AlignHCenter)

        self.bottom_panel_number = QtGui.QLineEdit(parent=self.bottom_number_panel)
        self.bottom_panel_number.setGeometry(int(self.bottom_number_panel.width() * 0.2), int(self.bottom_number_panel.height() * 0.6), int(self.bottom_number_panel.width() * 0.6), 100)
        self.bottom_panel_number.setFont(QtGui.QFont('Ubuntu', 40))
        self.bottom_panel_number.setAlignment(QtCore.Qt.AlignHCenter)
        self.bottom_panel_number.textChanged.connect(self.change_number)
        self.bottom_panel_number.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,20%) ; border-radius: 8px;}")
        self.bottom_panel_number.setBackgroundRole(QtGui.QPalette.ColorRole(0))

        self.bottom_panel_yes = QtGui.QWidget(parent=self.bottom_number_panel)
        self.bottom_panel_yes.setGeometry(int(self.bottom_number_panel.width() * 0.2), int(self.bottom_number_panel.height() * 0.6), int(self.bottom_number_panel.width() * 0.29), 100)
        self.bottom_panel_yes.setShown(False)

        self.bottom_panel_yes_border = QtGui.QWidget(parent=self.bottom_panel_yes)
        self.bottom_panel_yes_border.setGeometry(0,0,self.bottom_panel_yes.width(), self.bottom_panel_yes.height())
        self.bottom_panel_yes_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,20%) ; border-radius: 8px;}")

        self.bottom_panel_yes_label = QtGui.QLabel(parent=self.bottom_panel_yes)
        self.bottom_panel_yes_label.setFont(QtGui.QFont('Ubuntu', 22, 75))
        self.bottom_panel_yes_label.setText(u'Sim')
        self.bottom_panel_yes_label.setGeometry(0, 0, self.bottom_panel_yes.width(), self.bottom_panel_yes.height())
        self.bottom_panel_yes_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignCenter)

        self.bottom_panel_no = QtGui.QWidget(parent=self.bottom_number_panel)
        self.bottom_panel_no.setGeometry(int(self.bottom_number_panel.width() * 0.51), int(self.bottom_number_panel.height() * 0.6), int(self.bottom_number_panel.width() * 0.29), 100)
        self.bottom_panel_no.setShown(False)

        self.bottom_panel_no_border = QtGui.QWidget(parent=self.bottom_panel_no)
        self.bottom_panel_no_border.setGeometry(0,0,self.bottom_panel_no.width(), self.bottom_panel_no.height())
        self.bottom_panel_no_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,20%) ; border-radius: 8px;}")

        self.bottom_panel_no_label = QtGui.QLabel(parent=self.bottom_panel_no)
        self.bottom_panel_no_label.setFont(QtGui.QFont('Ubuntu', 22, 75))
        self.bottom_panel_no_label.setText(u'Não')
        self.bottom_panel_no_label.setGeometry(0, 0, self.bottom_panel_no.width(), self.bottom_panel_no.height())
        self.bottom_panel_no_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignCenter)

        self.top_panel = QtGui.QWidget(parent=self)
        self.top_panel.setGeometry(0, 0, QtGui.QDesktopWidget().screenGeometry().width(), int(QtGui.QDesktopWidget().screenGeometry().height() * 0.6))
        self.top_panel_animation = QtCore.QPropertyAnimation(self.top_panel, 'geometry')
        self.top_panel_animation.setEasingCurve(QtCore.QEasingCurve(26))

        self.top_panel_background = QtGui.QLabel(parent=self.top_panel)
        self.top_panel_background.setGeometry(0, 0, QtGui.QDesktopWidget().screenGeometry().width(), int(QtGui.QDesktopWidget().screenGeometry().height() * 0.6))
        self.top_panel_background.setPixmap(QtGui.QPixmap(os.path.join(dirpathSoftware, 'topo.png')))
        self.top_panel_background.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignBottom)

        self.top_panel_title = QtGui.QLabel(parent=self.top_panel)
        self.top_panel_title.setText(mainConfigFile.get('geral', 'titulo-principal'))
        self.top_panel_title.setFont(QtGui.QFont('Ubuntu', 50, 75))
        self.top_panel_title.setGeometry(0, int(self.top_panel.height() * 0.35), QtGui.QDesktopWidget().screenGeometry().width(), int(self.top_panel.height() * 0.2))
        self.top_panel_title.setAlignment(QtCore.Qt.AlignHCenter)

        self.top_panel_title_place = QtGui.QLabel(parent=self.top_panel)
        self.top_panel_title_place.setText(unicode(mainConfigFile.get('geral', 'subtitulo'), 'utf-8'))
        self.top_panel_title_place.setFont(QtGui.QFont('Ubuntu', 20))
        self.top_panel_title_place.setGeometry(0, int(self.top_panel.height() * 0.55), QtGui.QDesktopWidget().screenGeometry().width(), int(self.top_panel.height() * 0.1))
        self.top_panel_title_place.setAlignment(QtCore.Qt.AlignHCenter)

        self.top_panel_sound_icon = QtGui.QLabel(parent=self.top_panel)
        self.top_panel_sound_icon.setPixmap(QtGui.QPixmap(os.path.join(dirpathSoftware, 'voice.png')))
        self.top_panel_sound_icon.setGeometry(int(QtGui.QDesktopWidget().screenGeometry().width() / 2) - 24, int(self.top_panel.height()) - 100, 48, 48)
        self.top_panel_sound_icon.setShown(False)

        self.configuration_panel = QtGui.QWidget(parent=self.bottom_panel)
        self.configuration_panel.setGeometry(0, 0, self.bottom_panel.width(), self.bottom_panel.height())
        self.configuration_panel.setShown(False)

        self.configuration_panel_border = QtGui.QWidget(parent=self.configuration_panel)
        self.configuration_panel_border.setGeometry(int(self.configuration_panel.width() * 0.05), int(self.configuration_panel.height() * 0.05), int(self.configuration_panel.width() - int(self.configuration_panel.width() * 0.1)), int(self.configuration_panel.height() - int(self.configuration_panel.height() * 0.1)))
        self.configuration_panel_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,20%) ; border-radius: 8px;}")

        self.configuration_panel_transparency = QtGui.QGraphicsOpacityEffect()
        self.configuration_panel.setGraphicsEffect(self.configuration_panel_transparency)
        self.configuration_panel_transparency.setOpacity(0.0)

        self.configuration_panel_transparency_animation = QtCore.QPropertyAnimation(self.configuration_panel_transparency, 'opacity')
        self.configuration_panel_transparency_animation.setEasingCurve(QtCore.QEasingCurve(26))

        self.configuration_panel_tabs = QtGui.QTabWidget(parent=self.configuration_panel)
        self.configuration_panel_tabs.setGeometry(self.configuration_panel.width() * 0.1, self.configuration_panel.height() * 0.1, self.configuration_panel.width() * 0.8, self.configuration_panel.height() * 0.8)

        self.configuration_panel_members = QtGui.QWidget(parent=self.configuration_panel)

        self.configuration_panel_new_products = QtGui.QWidget(parent=self.configuration_panel)

        self.configuration_panel_products = QtGui.QWidget(parent=self.configuration_panel)

        self.configuration_panel_balance = QtGui.QWidget(parent=self.configuration_panel)

        self.configuration_panel_configuration = QtGui.QWidget(parent=self.configuration_panel)

        self.configuration_panel_tabs.addTab(self.configuration_panel_members, u"Membros")
        self.configuration_panel_tabs.addTab(self.configuration_panel_new_products, u"Chegada de produtos")
        self.configuration_panel_tabs.addTab(self.configuration_panel_products, u"Produtos em estoque")
        self.configuration_panel_tabs.addTab(self.configuration_panel_balance, u"Relatórios")
        self.configuration_panel_tabs.addTab(self.configuration_panel_configuration, u"Configurações do programa")

        self.configuration_panel_members_list = QtGui.QListWidget(parent=self.configuration_panel_members)
        self.configuration_panel_members_list.setGeometry(10, 10, self.configuration_panel_tabs.width() * 0.35, int(self.configuration_panel_tabs.height() - 34) - 20)
        self.configuration_panel_members_list.setSelectionMode(1)
        self.configuration_panel_members_list.currentItemChanged.connect(self.configutation_panel_members_list_change)

        self.configuration_panel_members_edit_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("gtk-edit"), u'Editar', parent=self.configuration_panel_members)
        self.configuration_panel_members_edit_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 10, int(self.configuration_panel_tabs.width() * 0.15), 30)
        self.configuration_panel_members_edit_button.clicked.connect(self.configuration_panel_members_edit)
        self.configuration_panel_members_edit_button.setEnabled(False)

        self.configuration_panel_members_add_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("list-add"), u'Adicionar nome', parent=self.configuration_panel_members)
        self.configuration_panel_members_add_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.50) + 25, 10, int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_members_add_button.clicked.connect(self.configuration_panel_members_add)

        self.configuration_panel_members_remove_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("list-remove"), u'Remover nome', parent=self.configuration_panel_members)
        self.configuration_panel_members_remove_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.72) + 30, 10, int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_members_remove_button.clicked.connect(self.configuration_panel_members_remove)
        self.configuration_panel_members_remove_button.setEnabled(False)

        self.configuration_panel_members_confirm_edit_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("dialog-apply"), u'Confirmar edição', parent=self.configuration_panel_members)
        self.configuration_panel_members_confirm_edit_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.72) + 30, int(self.configuration_panel_tabs.height() * 0.5), int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_members_confirm_edit_button.clicked.connect(self.configuration_panel_members_confirm_edit)
        self.configuration_panel_members_confirm_edit_button.setShown(False)

        self.configuration_panel_members_confirm_add_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("dialog-apply"), u'Confirmar adição', parent=self.configuration_panel_members)
        self.configuration_panel_members_confirm_add_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.72) + 30, int(self.configuration_panel_tabs.height() * 0.5), int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_members_confirm_add_button.clicked.connect(self.configuration_panel_members_confirm_add)
        self.configuration_panel_members_confirm_add_button.setShown(False)

        self.configuration_panel_members_cancel_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("dialog-cancel"), u'Cancelar', parent=self.configuration_panel_members)
        self.configuration_panel_members_cancel_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, int(self.configuration_panel_tabs.height() * 0.5), int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_members_cancel_button.clicked.connect(self.configuration_panel_members_cancel)
        self.configuration_panel_members_cancel_button.setShown(False)

        self.configuration_panel_members_name_title = QtGui.QLabel(parent=self.configuration_panel_members)
        self.configuration_panel_members_name_title.setText(u"NOME")
        self.configuration_panel_members_name_title.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 50, int(self.configuration_panel_tabs.width() * 0.65) - 30, 15)
        self.configuration_panel_members_name_title.setFont(QtGui.QFont('Ubuntu', 8, 75))
        self.configuration_panel_members_name_title.setShown(False)

        self.configuration_panel_members_name_field = QtGui.QLineEdit(parent=self.configuration_panel_members)
        self.configuration_panel_members_name_field.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 65, int(self.configuration_panel_tabs.width() * 0.65) - 30, 25)
        self.configuration_panel_members_name_field.setShown(False)

        self.configuration_panel_members_number_title = QtGui.QLabel(parent=self.configuration_panel_members)
        self.configuration_panel_members_number_title.setText(u"NÚMERO")
        self.configuration_panel_members_number_title.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 100, int(self.configuration_panel_tabs.width() * 0.30) - 30, 15)
        self.configuration_panel_members_number_title.setFont(QtGui.QFont('Ubuntu', 8))
        self.configuration_panel_members_number_title.setShown(False)

        self.configuration_panel_members_number = QtGui.QSpinBox(parent=self.configuration_panel_members)
        self.configuration_panel_members_number.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 115, int(self.configuration_panel_tabs.width() * 0.30) - 30, 25)
        self.configuration_panel_members_number.setMinimum(0)
        self.configuration_panel_members_number.setMaximum(9999999)
        self.configuration_panel_members_number.setShown(False)
        self.configuration_panel_members_number.valueChanged.connect(self.configuration_panel_members_change_number)

        self.configuration_panel_members_number_warning = QtGui.QLabel(parent=self.configuration_panel_members)
        self.configuration_panel_members_number_warning.setGeometry(int(self.configuration_panel_tabs.width() * 0.65), 115, int(self.configuration_panel_tabs.width() * 0.65) - 30, 25)
        self.configuration_panel_members_number_warning.setFont(QtGui.QFont('Ubuntu', 8))
        self.configuration_panel_members_number_warning.setStyleSheet("QWidget { color: rgb(255,0,0) }")
        self.configuration_panel_members_number_warning.setShown(False)
        self.configuration_panel_members_number_warning.setWordWrap(True)
        self.configuration_panel_members_number_warning.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)

        self.configuration_panel_new_products_code_title = QtGui.QLabel(parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_code_title.setText(u"CÓDIGO DO PRODUTO QUE CHEGOU")
        self.configuration_panel_new_products_code_title.setGeometry(10, 10, self.configuration_panel_tabs.width() * 0.4, 15)
        self.configuration_panel_new_products_code_title.setFont(QtGui.QFont('Ubuntu', 8, 75))

        self.configuration_panel_new_products_code = QtGui.QLineEdit(parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_code.setGeometry(10, 25, self.configuration_panel_tabs.width() * 0.2, 25)
        self.configuration_panel_new_products_code.setInputMask('0'*13)
        self.configuration_panel_new_products_code.editingFinished.connect(self.configuration_panel_new_products_code_change)

        self.configuration_panel_new_products_code_title2 = QtGui.QLabel('', parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_code_title2.setGeometry((self.configuration_panel_tabs.width() * 0.2) + 20, 10, self.configuration_panel_tabs.width() * 0.8, 40)
        self.configuration_panel_new_products_code_title2.setWordWrap(True)
        self.configuration_panel_new_products_code_title2.setShown(False)
        self.configuration_panel_new_products_code_title2.setFont(QtGui.QFont('Ubuntu', 10))

        self.configuration_panel_new_products_code_title3 = QtGui.QLabel('', parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_code_title3.setGeometry(10, 50, self.configuration_panel_tabs.width() - 20, 25)
        self.configuration_panel_new_products_code_title3.setWordWrap(True)
        self.configuration_panel_new_products_code_title3.setShown(False)
        self.configuration_panel_new_products_code_title3.setFont(QtGui.QFont('Ubuntu', 10))

        self.configuration_panel_new_products_number_title = QtGui.QLabel(parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_number_title.setText(u"QUANTIDADE DE ITENS")
        self.configuration_panel_new_products_number_title.setGeometry(10, 100, self.configuration_panel_tabs.width() * 0.2, 15)
        self.configuration_panel_new_products_number_title.setShown(False)
        self.configuration_panel_new_products_number_title.setFont(QtGui.QFont('Ubuntu', 8))

        self.configuration_panel_new_products_number = QtGui.QSpinBox(parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_number.setGeometry(10, 115, self.configuration_panel_tabs.width() * 0.2, 25)
        self.configuration_panel_new_products_number.setMinimum(1)
        self.configuration_panel_new_products_number.setMaximum(999)
        self.configuration_panel_new_products_number.setShown(False)
        self.configuration_panel_new_products_number.valueChanged.connect(self.configuration_panel_new_products_numbers_change)

        self.configuration_panel_new_products_price_title = QtGui.QLabel(parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_price_title.setText(u"PREÇO DESSA QUANTIDADE")
        self.configuration_panel_new_products_price_title.setGeometry((self.configuration_panel_tabs.width() * 0.2) + 20, 100, self.configuration_panel_tabs.width() * 0.2, 15)
        self.configuration_panel_new_products_price_title.setShown(False)
        self.configuration_panel_new_products_price_title.setFont(QtGui.QFont('Ubuntu', 8))

        self.configuration_panel_new_products_price = QtGui.QLineEdit(parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_price.setGeometry((self.configuration_panel_tabs.width() * 0.2) + 20, 115, self.configuration_panel_tabs.width() * 0.2, 25)
        self.configuration_panel_new_products_price.setInputMask('000,00')
        self.configuration_panel_new_products_price.setShown(False)
        self.configuration_panel_new_products_price.textChanged.connect(self.configuration_panel_new_products_numbers_change)

        self.configuration_panel_new_products_cancel = QtGui.QPushButton(QtGui.QIcon.fromTheme("dialog-cancel"), u'Cancelar', parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_cancel.setGeometry((self.configuration_panel_tabs.width() * 0.6), 100, int(self.configuration_panel_tabs.width() * 0.2)-10, 35)
        self.configuration_panel_new_products_cancel.setIconSize(QtCore.QSize(24,24))
        self.configuration_panel_new_products_cancel.setShown(False)
        self.configuration_panel_new_products_cancel.clicked.connect(self.configuration_panel_new_products_cancel_click)

        self.configuration_panel_new_products_confirm = QtGui.QPushButton(QtGui.QIcon.fromTheme("list-add"), u'Adicionar', parent=self.configuration_panel_new_products)
        self.configuration_panel_new_products_confirm.setGeometry((self.configuration_panel_tabs.width() * 0.8), 100, int(self.configuration_panel_tabs.width() * 0.2)-10, 35)
        self.configuration_panel_new_products_confirm.setIconSize(QtCore.QSize(24,24))
        self.configuration_panel_new_products_confirm.setShown(False)
        self.configuration_panel_new_products_confirm.clicked.connect(self.configuration_panel_new_products_confirm_click)



        self.configuration_panel_products_list = QtGui.QListWidget(parent=self.configuration_panel_products)
        self.configuration_panel_products_list.setGeometry(10, 10, self.configuration_panel_tabs.width() * 0.35, int(self.configuration_panel_tabs.height() - 34) - 20)
        self.configuration_panel_products_list.setSelectionMode(1)
        self.configuration_panel_products_list.currentItemChanged.connect(self.configutation_panel_products_list_change)

        self.configuration_panel_products_edit_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("gtk-edit"), u'Editar', parent=self.configuration_panel_products)
        self.configuration_panel_products_edit_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 10, int(self.configuration_panel_tabs.width() * 0.15), 30)
        self.configuration_panel_products_edit_button.clicked.connect(self.configuration_panel_products_edit)
        self.configuration_panel_products_edit_button.setEnabled(False)

        self.configuration_panel_products_add_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("list-add"), u'Adicionar produto', parent=self.configuration_panel_products)
        self.configuration_panel_products_add_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.50) + 25, 10, int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_products_add_button.clicked.connect(self.configuration_panel_products_add)

        self.configuration_panel_products_remove_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("list-remove"), u'Remover produto', parent=self.configuration_panel_products)
        self.configuration_panel_products_remove_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.72) + 30, 10, int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_products_remove_button.clicked.connect(self.configuration_panel_products_remove)
        self.configuration_panel_products_remove_button.setEnabled(False)

        self.configuration_panel_products_confirm_edit_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("dialog-apply"), u'Confirmar edição', parent=self.configuration_panel_products)
        self.configuration_panel_products_confirm_edit_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.72) + 30, int(self.configuration_panel_tabs.height() * 0.5), int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_products_confirm_edit_button.clicked.connect(self.configuration_panel_products_confirm_edit)
        self.configuration_panel_products_confirm_edit_button.setShown(False)

        self.configuration_panel_products_confirm_add_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("dialog-apply"), u'Confirmar adição', parent=self.configuration_panel_products)
        self.configuration_panel_products_confirm_add_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.72) + 30, int(self.configuration_panel_tabs.height() * 0.5), int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_products_confirm_add_button.clicked.connect(self.configuration_panel_products_confirm_add)
        self.configuration_panel_products_confirm_add_button.setShown(False)

        self.configuration_panel_products_cancel_button = QtGui.QPushButton(QtGui.QIcon.fromTheme("dialog-cancel"), u'Cancelar', parent=self.configuration_panel_products)
        self.configuration_panel_products_cancel_button.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, int(self.configuration_panel_tabs.height() * 0.5), int(self.configuration_panel_tabs.width() * 0.22), 30)
        self.configuration_panel_products_cancel_button.clicked.connect(self.configuration_panel_products_cancel)
        self.configuration_panel_products_cancel_button.setShown(False)

        self.configuration_panel_products_code_title = QtGui.QLabel(parent=self.configuration_panel_products)
        self.configuration_panel_products_code_title.setText(u"CÓDIGO")
        self.configuration_panel_products_code_title.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 50, int(self.configuration_panel_tabs.width() * 0.25) - 30, 15)
        self.configuration_panel_products_code_title.setFont(QtGui.QFont('Ubuntu', 8))
        self.configuration_panel_products_code_title.setShown(False)

        self.configuration_panel_products_code_field = QtGui.QLineEdit(parent=self.configuration_panel_products)
        self.configuration_panel_products_code_field.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 65, int(self.configuration_panel_tabs.width() * 0.25) - 30, 25)
        self.configuration_panel_products_code_field.setShown(False)
        self.configuration_panel_products_code_field.setInputMask('0'*13)
        self.configuration_panel_products_code_field.editingFinished.connect(self.configuration_panel_products_code_field_change)

        self.configuration_panel_products_name_title = QtGui.QLabel(parent=self.configuration_panel_products)
        self.configuration_panel_products_name_title.setText(u"NOME DO PRODUTO")
        self.configuration_panel_products_name_title.setGeometry(int(self.configuration_panel_tabs.width() * 0.60), 50, int(self.configuration_panel_tabs.width() * 0.40) - 30, 15)
        self.configuration_panel_products_name_title.setFont(QtGui.QFont('Ubuntu', 8, 75))
        self.configuration_panel_products_name_title.setShown(False)

        self.configuration_panel_products_name_field = QtGui.QLineEdit(parent=self.configuration_panel_products)
        self.configuration_panel_products_name_field.setGeometry(int(self.configuration_panel_tabs.width() * 0.60), 65, int(self.configuration_panel_tabs.width() * 0.40) - 30, 25)
        self.configuration_panel_products_name_field.setShown(False)

        self.configuration_panel_products_description_title = QtGui.QLabel(parent=self.configuration_panel_products)
        self.configuration_panel_products_description_title.setText(u"DESCRIÇÃO")
        self.configuration_panel_products_description_title.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 100, int(self.configuration_panel_tabs.width() * 0.65) - 50, 15)
        self.configuration_panel_products_description_title.setFont(QtGui.QFont('Ubuntu', 8))
        self.configuration_panel_products_description_title.setShown(False)

        self.configuration_panel_products_description_field = QtGui.QLineEdit(parent=self.configuration_panel_products)
        self.configuration_panel_products_description_field.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 115, int(self.configuration_panel_tabs.width() * 0.65) - 50, 25)
        self.configuration_panel_products_description_field.setShown(False)

        self.configuration_panel_products_stock_title = QtGui.QLabel(parent=self.configuration_panel_products)
        self.configuration_panel_products_stock_title.setText(u"EM ESTOQUE")
        self.configuration_panel_products_stock_title.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 150, int(self.configuration_panel_tabs.width() * 0.20) - 30, 15)
        self.configuration_panel_products_stock_title.setFont(QtGui.QFont('Ubuntu', 8))
        self.configuration_panel_products_stock_title.setShown(False)

        self.configuration_panel_products_stock = QtGui.QSpinBox(parent=self.configuration_panel_products)
        self.configuration_panel_products_stock.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 165, int(self.configuration_panel_tabs.width() * 0.20) - 30, 25)
        self.configuration_panel_products_stock.setMinimum(0)
        self.configuration_panel_products_stock.setMaximum(999)
        self.configuration_panel_products_stock.setShown(False)

        self.configuration_panel_products_price_title = QtGui.QLabel(parent=self.configuration_panel_products)
        self.configuration_panel_products_price_title.setText(u"PREÇO")
        self.configuration_panel_products_price_title.setGeometry(int(self.configuration_panel_tabs.width() * 0.55) + 20, 150, int(self.configuration_panel_tabs.width() * 0.45) - 50, 15)
        self.configuration_panel_products_price_title.setFont(QtGui.QFont('Ubuntu', 8))
        self.configuration_panel_products_price_title.setShown(False)

        self.configuration_panel_products_price_title_rs = QtGui.QLabel(parent=self.configuration_panel_products)
        self.configuration_panel_products_price_title_rs.setText(u"R$")
        self.configuration_panel_products_price_title_rs.setGeometry(int(self.configuration_panel_tabs.width() * 0.55) + 20, 165, int(self.configuration_panel_tabs.width() * 0.1) - 50, 25)
        self.configuration_panel_products_price_title_rs.setFont(QtGui.QFont('Ubuntu', 14))
        self.configuration_panel_products_price_title_rs.setShown(False)

        self.configuration_panel_products_price = QtGui.QLineEdit(parent=self.configuration_panel_products)
        self.configuration_panel_products_price.setGeometry(int(self.configuration_panel_tabs.width() * 0.60) + 20, 165, int(self.configuration_panel_tabs.width() * 0.4) - 50, 25)
        self.configuration_panel_products_price.setInputMask('000,00')
        self.configuration_panel_products_price.setShown(False)
        self.configuration_panel_products_price.editingFinished.connect(self.configuration_panel_products_change_price)

        self.configuration_panel_products_variableprice = QtGui.QCheckBox(u'Preço variável', parent=self.configuration_panel_products)
        self.configuration_panel_products_variableprice.setGeometry(int(self.configuration_panel_tabs.width() * 0.35) + 20, 200, int(self.configuration_panel_tabs.width() * 0.20) - 30, 25)
        self.configuration_panel_products_variableprice.clicked.connect(self.configuration_panel_products_variableprice_clicked)
        self.configuration_panel_products_variableprice.setShown(False)

        self.configuration_panel_balance_individual_panel = QtGui.QWidget(parent=self.configuration_panel_balance)
        self.configuration_panel_balance_individual_panel.setGeometry(10, 10, int(self.configuration_panel_tabs.width()) - 20, self.configuration_panel_tabs.height() * 0.2)

        self.configuration_panel_balance_individual_panel_title = QtGui.QLabel(u'Relatórios individuais', parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_individual_panel_title.setGeometry(0, 0, self.configuration_panel_balance_individual_panel.width(), 20)
        self.configuration_panel_balance_individual_panel_title.setFont(QtGui.QFont('Ubuntu', 14, 75))

        self.configuration_panel_balance_individual_panel_option_all = QtGui.QRadioButton(u"Todos os membros", parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_individual_panel_option_all.setGeometry(0, 30, self.configuration_panel_balance_individual_panel.width() * 0.2, 25)
        self.configuration_panel_balance_individual_panel_option_all.clicked.connect(self.configuration_panel_balance_change_individual_option)

        self.configuration_panel_balance_individual_panel_option_one = QtGui.QRadioButton(u"Apenas um:", parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_individual_panel_option_one.setGeometry(self.configuration_panel_balance_individual_panel.width() * 0.2, 30, self.configuration_panel_balance_individual_panel.width() * 0.2, 25)
        self.configuration_panel_balance_individual_panel_option_one.clicked.connect(self.configuration_panel_balance_change_individual_option)

        self.configuration_panel_balance_individual_panel_option_one_name = QtGui.QSpinBox(parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_individual_panel_option_one_name.setGeometry(self.configuration_panel_balance_individual_panel.width() * 0.4, 30, self.configuration_panel_balance_individual_panel.width() * 0.2, 25)
        self.configuration_panel_balance_individual_panel_option_one_name.setMinimum(0)
        self.configuration_panel_balance_individual_panel_option_one_name.setMaximum(9999999)
        self.configuration_panel_balance_individual_panel_option_one_name.setEnabled(False)

        self.configuration_panel_balance_individual_panel_date_title = QtGui.QLabel(u"Período:", parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_individual_panel_date_title.setGeometry(0, 70, self.configuration_panel_balance_individual_panel.width() * 0.1, 25)
        self.configuration_panel_balance_individual_panel_date_title.setFont(QtGui.QFont('Ubuntu', 10))

        self.configuration_panel_balance_individual_panel_date_start = QtGui.QDateTimeEdit(parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_individual_panel_date_start.setGeometry(self.configuration_panel_balance_individual_panel.width() * 0.1, 70, self.configuration_panel_balance_individual_panel.width() * 0.2, 25)
        self.configuration_panel_balance_individual_panel_date_start.setDisplayFormat("dd/MM/yyyy - hh:mm")

        self.configuration_panel_balance_individual_panel_date_title2 = QtGui.QLabel(u"até", parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_individual_panel_date_title2.setGeometry(self.configuration_panel_balance_individual_panel.width() * 0.3, 70, self.configuration_panel_balance_individual_panel.width() * 0.1, 25)
        self.configuration_panel_balance_individual_panel_date_title2.setFont(QtGui.QFont('Ubuntu', 10))
        self.configuration_panel_balance_individual_panel_date_title2.setAlignment(QtCore.Qt.AlignCenter)

        self.configuration_panel_balance_individual_panel_date_end = QtGui.QDateTimeEdit(parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_individual_panel_date_end.setGeometry(self.configuration_panel_balance_individual_panel.width() * 0.4, 70, self.configuration_panel_balance_individual_panel.width() * 0.2, 25)
        self.configuration_panel_balance_individual_panel_date_end.setMinimumDateTime(QtCore.QDateTime.fromString(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d") + datetime.datetime.now().strftime("%H") + datetime.datetime.now().strftime("%M"), "yyyyMMddhhmm"))
        self.configuration_panel_balance_individual_panel_date_end.setDisplayFormat("dd/MM/yyyy - hh:mm")

        self.configuration_panel_balance_button_individual_pdf = QtGui.QPushButton(QtGui.QIcon.fromTheme("document-export"), u'Relatórios em PDF', parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_button_individual_pdf.setGeometry(int(self.configuration_panel_balance_individual_panel.width()) * 0.7, 30, int(self.configuration_panel_balance_individual_panel.width() * 0.3), 35)
        self.configuration_panel_balance_button_individual_pdf.setIconSize(QtCore.QSize(24,24))
        self.configuration_panel_balance_button_individual_pdf.clicked.connect(self.export_PDF)
        self.configuration_panel_balance_button_individual_pdf.setEnabled(False)

        self.configuration_panel_balance_button_individual_csv = QtGui.QPushButton(QtGui.QIcon.fromTheme("document-export"), u'Relatórios em CSV', parent=self.configuration_panel_balance_individual_panel)
        self.configuration_panel_balance_button_individual_csv.setGeometry(int(self.configuration_panel_balance_individual_panel.width()) * 0.7, 75, int(self.configuration_panel_balance_individual_panel.width() * 0.3), 35)
        self.configuration_panel_balance_button_individual_csv.setIconSize(QtCore.QSize(24,24))
        self.configuration_panel_balance_button_individual_csv.clicked.connect(self.export_CSV)
        self.configuration_panel_balance_button_individual_csv.setEnabled(False)


        self.configuration_panel_balance_close_panel = QtGui.QWidget(parent=self.configuration_panel_balance)
        self.configuration_panel_balance_close_panel.setGeometry(10, (self.configuration_panel_tabs.height() * 0.2) + 20, int(self.configuration_panel_tabs.width()) - 20, self.configuration_panel_tabs.height() * 0.2)

        self.configuration_panel_balance_close_panel_title = QtGui.QLabel(u'Fechar todas as contas', parent=self.configuration_panel_balance_close_panel)
        self.configuration_panel_balance_close_panel_title.setGeometry(0, 0, self.configuration_panel_balance_close_panel.width(), 20)
        self.configuration_panel_balance_close_panel_title.setFont(QtGui.QFont('Ubuntu', 14, 75))

        self.configuration_panel_balance_close_panel_title2 = QtGui.QLabel(u'Ao clicar no botão "Fechar contas", o programa não exibirá na tela do usuário os valores somados das últimas compras. Isso não exclui nenhum dado do programa.', parent=self.configuration_panel_balance_close_panel)
        self.configuration_panel_balance_close_panel_title2.setGeometry(0, 30, self.configuration_panel_balance_close_panel.width() * 0.5, 50)
        self.configuration_panel_balance_close_panel_title2.setWordWrap(True)
        self.configuration_panel_balance_close_panel_title2.setFont(QtGui.QFont('Ubuntu', 10))

        self.configuration_panel_balance_button_close_balance = QtGui.QPushButton(QtGui.QIcon.fromTheme("dialog-apply"), u'Fechar contas', parent=self.configuration_panel_balance_close_panel)
        self.configuration_panel_balance_button_close_balance.setGeometry(int(self.configuration_panel_balance_close_panel.width()) * 0.7, 30, int(self.configuration_panel_balance_close_panel.width() * 0.3), 35)
        self.configuration_panel_balance_button_close_balance.setIconSize(QtCore.QSize(24,24))
        self.configuration_panel_balance_button_close_balance.clicked.connect(self.close_balance)

        self.configuration_panel_balance_products_panel = QtGui.QWidget(parent=self.configuration_panel_balance)
        self.configuration_panel_balance_products_panel.setGeometry(10, (self.configuration_panel_tabs.height() * 0.4) + 20, int(self.configuration_panel_tabs.width()) - 20, self.configuration_panel_tabs.height() * 0.2)

        self.configuration_panel_balance_products_panel_title = QtGui.QLabel(u'Produtos em Estoque', parent=self.configuration_panel_balance_products_panel)
        self.configuration_panel_balance_products_panel_title.setGeometry(0, 0, self.configuration_panel_balance_close_panel.width(), 20)
        self.configuration_panel_balance_products_panel_title.setFont(QtGui.QFont('Ubuntu', 14, 75))

        self.configuration_panel_balance_products_panel_title2 = QtGui.QLabel(u'O relatório exibirá a soma dos valores que saíram da lojinha no período de:', parent=self.configuration_panel_balance_products_panel)
        self.configuration_panel_balance_products_panel_title2.setGeometry(0, 30, self.configuration_panel_balance_products_panel.width() * 0.5, 30)
        self.configuration_panel_balance_products_panel_title2.setWordWrap(True)
        self.configuration_panel_balance_products_panel_title2.setFont(QtGui.QFont('Ubuntu', 10))

        self.configuration_panel_balance_products_panel_date_start = QtGui.QDateEdit(parent=self.configuration_panel_balance_products_panel)
        self.configuration_panel_balance_products_panel_date_start.setGeometry(0, 70, self.configuration_panel_balance_products_panel.width() * 0.2, 25)
        self.configuration_panel_balance_products_panel_date_start.setDisplayFormat("dd/MM/yyyy - hh:mm")

        self.configuration_panel_balance_products_panel_date_title2 = QtGui.QLabel(u"até", parent=self.configuration_panel_balance_products_panel)
        self.configuration_panel_balance_products_panel_date_title2.setGeometry(self.configuration_panel_balance_products_panel.width() * 0.2, 70, self.configuration_panel_balance_products_panel.width() * 0.1, 25)
        self.configuration_panel_balance_products_panel_date_title2.setFont(QtGui.QFont('Ubuntu', 10))
        self.configuration_panel_balance_products_panel_date_title2.setAlignment(QtCore.Qt.AlignCenter)

        self.configuration_panel_balance_products_panel_date_end = QtGui.QDateTimeEdit(parent=self.configuration_panel_balance_products_panel)
        self.configuration_panel_balance_products_panel_date_end.setGeometry(self.configuration_panel_balance_products_panel.width() * 0.3, 70, self.configuration_panel_balance_products_panel.width() * 0.2, 25)
        self.configuration_panel_balance_products_panel_date_end.setMinimumDateTime(QtCore.QDateTime.fromString(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d") + datetime.datetime.now().strftime("%H") + datetime.datetime.now().strftime("%M"), "yyyyMMddhhmm"))
        self.configuration_panel_balance_products_panel_date_end.setDisplayFormat("dd/MM/yyyy - hh:mm")

        self.configuration_panel_balance_button_products_pdf = QtGui.QPushButton(QtGui.QIcon.fromTheme("document-export"), u'Relatório geral em PDF', parent=self.configuration_panel_balance_products_panel)
        self.configuration_panel_balance_button_products_pdf.setGeometry(int(self.configuration_panel_balance_products_panel.width()) * 0.7, 30, int(self.configuration_panel_balance_products_panel.width() * 0.3), 35)
        self.configuration_panel_balance_button_products_pdf.setIconSize(QtCore.QSize(24,24))
        self.configuration_panel_balance_button_products_pdf.clicked.connect(self.products_export_PDF)

        self.configuration_panel_balance_button_products_csv = QtGui.QPushButton(QtGui.QIcon.fromTheme("document-export"), u'Relatório geral em CSV', parent=self.configuration_panel_balance_products_panel)
        self.configuration_panel_balance_button_products_csv.setGeometry(int(self.configuration_panel_balance_products_panel.width()) * 0.7, 75, int(self.configuration_panel_balance_products_panel.width() * 0.3), 35)
        self.configuration_panel_balance_button_products_csv.setIconSize(QtCore.QSize(24,24))
        self.configuration_panel_balance_button_products_csv.clicked.connect(self.products_export_CSV)

        self.configuration_panel_configuration_program_name_title1 = QtGui.QLabel(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_name_title1.setText(u"TÍTULO PRINCIPAL DA TELA INICIAL")
        self.configuration_panel_configuration_program_name_title1.setGeometry(10, 10, self.configuration_panel_tabs.width() * 0.3, 15)
        self.configuration_panel_configuration_program_name_title1.setFont(QtGui.QFont('Ubuntu', 8, 75))

        self.configuration_panel_configuration_program_name_field1 = QtGui.QLineEdit(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_name_field1.setGeometry(10, 25, self.configuration_panel_tabs.width() * 0.3, 25)
        self.configuration_panel_configuration_program_name_field1.setText(unicode(mainConfigFile.get('geral', 'titulo-principal'), 'utf-8'))
        self.configuration_panel_configuration_program_name_field1.editingFinished.connect(self.configuration_panel_configuration_program_name_field1_change)

        self.configuration_panel_configuration_program_name_title2 = QtGui.QLabel(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_name_title2.setText(u"SUBTÍTULO DA TELA INICIAL")
        self.configuration_panel_configuration_program_name_title2.setGeometry((self.configuration_panel_tabs.width() * 0.3) + 15, 10, (self.configuration_panel_tabs.width() * 0.7) - 25, 15)
        self.configuration_panel_configuration_program_name_title2.setFont(QtGui.QFont('Ubuntu', 8))

        self.configuration_panel_configuration_program_name_field2 = QtGui.QLineEdit(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_name_field2.setGeometry((self.configuration_panel_tabs.width() * 0.3) + 15, 25, (self.configuration_panel_tabs.width() * 0.7) - 25, 25)
        self.configuration_panel_configuration_program_name_field2.setText(unicode(mainConfigFile.get('geral', 'subtitulo'), 'utf-8'))
        self.configuration_panel_configuration_program_name_field2.editingFinished.connect(self.configuration_panel_configuration_program_name_field2_change)

        self.configuration_panel_configuration_program_donate_number_title = QtGui.QLabel(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_donate_number_title.setText(u'"MEMBRO" PARA DONATIVO')
        self.configuration_panel_configuration_program_donate_number_title.setGeometry(10, 90, self.configuration_panel_tabs.width() * 0.2, 15)
        self.configuration_panel_configuration_program_donate_number_title.setFont(QtGui.QFont('Ubuntu', 8))

        self.configuration_panel_configuration_program_donate_number = QtGui.QSpinBox(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_donate_number.setGeometry(10, 105, self.configuration_panel_tabs.width() * 0.2, 25)
        self.configuration_panel_configuration_program_donate_number.setMaximum(9999999)
        self.configuration_panel_configuration_program_donate_number.setValue(int(mainConfigFile.get('geral', 'membro-donativo')))
        self.configuration_panel_configuration_program_donate_number.editingFinished.connect(self.configuration_panel_configuration_program_donate_number_change)

        self.configuration_panel_configuration_program_members_digits_title = QtGui.QLabel(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_members_digits_title.setText(u"DÍGITOS DOS MEMBROS")
        self.configuration_panel_configuration_program_members_digits_title.setGeometry((self.configuration_panel_tabs.width() * 0.2) + 15, 90, self.configuration_panel_tabs.width() * 0.2, 15)
        self.configuration_panel_configuration_program_members_digits_title.setFont(QtGui.QFont('Ubuntu', 8))

        self.configuration_panel_configuration_program_members_digits = QtGui.QSpinBox(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_members_digits.setGeometry((self.configuration_panel_tabs.width() * 0.2) + 15, 105, self.configuration_panel_tabs.width() * 0.2, 25)
        self.configuration_panel_configuration_program_members_digits.setMinimum(1)
        self.configuration_panel_configuration_program_members_digits.setMaximum(99)
        self.configuration_panel_configuration_program_members_digits.setValue(int(mainConfigFile.get('geral', 'membro-digitos')))
        self.configuration_panel_configuration_program_members_digits.editingFinished.connect(self.configuration_panel_configuration_program_members_digits_change)

        self.configuration_panel_configuration_program_donate_number_warning = QtGui.QLabel(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_donate_number_warning.setGeometry((self.configuration_panel_tabs.width() * 0.4) + 15, 90, self.configuration_panel_tabs.width() * 0.4, 40)
        self.configuration_panel_configuration_program_donate_number_warning.setFont(QtGui.QFont('Ubuntu', 8))
        self.configuration_panel_configuration_program_donate_number_warning.setStyleSheet("QWidget { color: rgb(255,0,0) }")
        self.configuration_panel_configuration_program_donate_number_warning.setShown(False)
        self.configuration_panel_configuration_program_donate_number_warning.setWordWrap(True)
        self.configuration_panel_configuration_program_donate_number_warning.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)

        self.configuration_panel_configuration_program_change_password_title = QtGui.QLabel(u'Alterar senha do administrador:', parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_change_password_title.setGeometry(10, 170, int(self.configuration_panel_tabs.width() * 0.2) - 10, 40)
        self.configuration_panel_configuration_program_change_password_title.setWordWrap(True)
        self.configuration_panel_configuration_program_change_password_title.setFont(QtGui.QFont('Ubuntu', 10))

        self.configuration_panel_configuration_program_actual_password_title= QtGui.QLabel(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_actual_password_title.setText(u"SENHA ATUAL")
        self.configuration_panel_configuration_program_actual_password_title.setGeometry((self.configuration_panel_tabs.width() * 0.25) + 10, 170, (self.configuration_panel_tabs.width() * 0.3) - 20, 15)
        self.configuration_panel_configuration_program_actual_password_title.setFont(QtGui.QFont('Ubuntu', 8))

        self.configuration_panel_configuration_program_actual_password = QtGui.QLineEdit(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_actual_password.setGeometry((self.configuration_panel_tabs.width() * 0.25) + 10, 185, (self.configuration_panel_tabs.width() * 0.25) - 20, 25)
        self.configuration_panel_configuration_program_actual_password.setEchoMode(2)
        self.configuration_panel_configuration_program_actual_password.textChanged.connect(self.configuration_panel_configuration_program_actual_password_change)

        self.configuration_panel_configuration_program_new1_password_title= QtGui.QLabel(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_new1_password_title.setText(u"NOVA SENHA")
        self.configuration_panel_configuration_program_new1_password_title.setGeometry((self.configuration_panel_tabs.width() * 0.5) + 10, 170, (self.configuration_panel_tabs.width() * 0.25) - 20, 15)
        self.configuration_panel_configuration_program_new1_password_title.setFont(QtGui.QFont('Ubuntu', 8))
        self.configuration_panel_configuration_program_new1_password_title.setEnabled(False)

        self.configuration_panel_configuration_program_new1_password = QtGui.QLineEdit(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_new1_password.setGeometry((self.configuration_panel_tabs.width() * 0.5) + 10, 185, (self.configuration_panel_tabs.width() * 0.25) - 20, 25)
        self.configuration_panel_configuration_program_new1_password.setEnabled(False)
        self.configuration_panel_configuration_program_new1_password.setEchoMode(2)

        self.configuration_panel_configuration_program_new2_password_title= QtGui.QLabel(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_new2_password_title.setText(u"DIGITE NOVAMENTE")
        self.configuration_panel_configuration_program_new2_password_title.setGeometry((self.configuration_panel_tabs.width() * 0.75) + 10, 170, (self.configuration_panel_tabs.width() * 0.25) - 20, 15)
        self.configuration_panel_configuration_program_new2_password_title.setFont(QtGui.QFont('Ubuntu', 8))
        self.configuration_panel_configuration_program_new2_password_title.setEnabled(False)

        self.configuration_panel_configuration_program_new2_password = QtGui.QLineEdit(parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_program_new2_password.setGeometry((self.configuration_panel_tabs.width() * 0.75) + 10, 185, (self.configuration_panel_tabs.width() * 0.25) - 20, 25)
        self.configuration_panel_configuration_program_new2_password.setEnabled(False)
        self.configuration_panel_configuration_program_new2_password.setEchoMode(2)
        self.configuration_panel_configuration_program_new2_password.textChanged.connect(self.configuration_panel_configuration_program_new_password_change)

        self.configuration_panel_configuration_backup_title = QtGui.QLabel(u'Cópias de segurança:', parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_backup_title.setGeometry(10, int(self.configuration_panel_tabs.height() - 70), int(self.configuration_panel_tabs.width() * 0.25) - 10, 30)
        self.configuration_panel_configuration_backup_title.setWordWrap(True)
        self.configuration_panel_configuration_backup_title.setFont(QtGui.QFont('Ubuntu', 10))

        self.configuration_panel_configuration_backup_make_package = QtGui.QPushButton(QtGui.QIcon.fromTheme("document-save"), u'Gerar Backup', parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_backup_make_package.setGeometry(int(self.configuration_panel_tabs.width() * 0.4), int(self.configuration_panel_tabs.height() - 70), int(self.configuration_panel_tabs.width() * 0.3) - 10, 30)
        self.configuration_panel_configuration_backup_make_package.clicked.connect(self.configuration_panel_configuration_backup_make_package_selected)

        self.configuration_panel_configuration_backup_open_package = QtGui.QPushButton(QtGui.QIcon.fromTheme("document-open"), u'Abrir Backup', parent=self.configuration_panel_configuration)
        self.configuration_panel_configuration_backup_open_package.setGeometry(int(self.configuration_panel_tabs.width() * 0.7),  int(self.configuration_panel_tabs.height() - 70), int(self.configuration_panel_tabs.width() * 0.3) - 10, 30)
        self.configuration_panel_configuration_backup_open_package.setEnabled(False)

        self.feed_list_of_members()
        self.feed_list_of_products()
        self.showFullScreen()
        self.bottom_panel_number.setFocus(1)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            if self.bottom_panel.y() == 0 or self.bottom_panel_number.echoMode() == 2 :
                self.clean_screen_to_welcome_screen()

        if event.key() == QtCore.Qt.Key_Comma:
            self.voice_change_option()

        if event.key() == QtCore.Qt.Key_Asterisk:
            self.negate_price()

        if event.key() == QtCore.Qt.Key_Space:
            if self.bottom_panel_yes.isVisible() == True or (self.bottom_panel.y() == QtGui.QDesktopWidget().screenGeometry().height() and self.main_product_code_panel_transparency.opacity() == 1):
                self.clean_screen_to_welcome_screen()
                self.parcial_price = 0.0

                if len(os.listdir('/media/')) >= 1:
                    self.generate_backup_package('/media/' + os.listdir('/media/')[0] + '/' + 'lojinha-backup-' + datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d") + datetime.datetime.now().strftime("%H") + datetime.datetime.now().strftime("%M"))


        if event.key() == QtCore.Qt.Key_Slash:
            self.confirm_price()

        if event.key() == QtCore.Qt.Key_Plus:
            if self.bottom_panel.y() == QtGui.QDesktopWidget().screenGeometry().height() and self.main_product_code_panel_transparency.opacity() == 0:

                if os.path.isfile(os.path.join(dirpathProducts, str(self.main_product_code_number.text()))):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, str(self.main_product_code_number.text())))
                    if int(self.main_product_details_panel_units_box_number.text()) < int(int(product_profile.get('geral', 'estoque'))):
                        self.main_product_details_panel_units_box_number.setText(str(int(self.main_product_details_panel_units_box_number.text()) + 1))
                        self.main_product_details_panel_units_box_unit.setText('UNIDADES')
                        self.main_product_details_panel_price.setText(u'R$ ' + str("%.2f" % float(float(product_profile.get('geral', 'preco').replace(',', '.')) * int(self.main_product_details_panel_units_box_number.text()))).replace('.', ','))
                        if self.voice:
                            subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', 'Mais um'])

                elif '.' in str(self.main_product_code_number.text()) or ',' in str(self.main_product_code_number.text()):
                    if len(str(self.main_product_code_number.text()).replace('.', ',').split(',')[1]) == 2:
                        self.main_product_details_panel_units_box_number.setText(str(int(self.main_product_details_panel_units_box_number.text()) + 1))
                        self.main_product_details_panel_units_box_unit.setText('UNIDADES')
                        self.main_product_details_panel_price.setText(u'R$ ' + str("%.2f" % float(float(self.main_product_code_number.text().replace(',', '.')) * int(self.main_product_details_panel_units_box_number.text()))).replace('.', ','))

        if event.key() == QtCore.Qt.Key_Minus:
            if int(self.main_product_details_panel_units_box_number.text()) > 1:

                if self.bottom_panel.y() == QtGui.QDesktopWidget().screenGeometry().height() and self.main_product_code_panel_transparency.opacity() == 0:
                    if os.path.isfile(os.path.join(dirpathProducts, str(self.main_product_code_number.text()))):
                        product_profile = ConfigParser.RawConfigParser()
                        product_profile.read(os.path.join(dirpathProducts, str(self.main_product_code_number.text())))
                        self.main_product_details_panel_units_box_number.setText(str(int(self.main_product_details_panel_units_box_number.text()) - 1))
                        if int(self.main_product_details_panel_units_box_number.text()) == 1:
                            self.main_product_details_panel_units_box_unit.setText('UNIDADE')
                        else:
                            self.main_product_details_panel_units_box_unit.setText('UNIDADES')
                        self.main_product_details_panel_price.setText(u'R$ ' + str("%.2f" % float(float(product_profile.get('geral', 'preco') .replace(',', '.')) * int(self.main_product_details_panel_units_box_number.text()))).replace('.', ','))
                        if self.voice:
                            subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', 'Menos um'])

                    elif '.' in str(self.main_product_code_number.text()) or ',' in str(self.main_product_code_number.text()):
                        if len(str(self.main_product_code_number.text()).replace('.', ',').split(',')[1]) == 2:
                            self.main_product_details_panel_units_box_number.setText(str(int(self.main_product_details_panel_units_box_number.text()) - 1))
                            if int(self.main_product_details_panel_units_box_number.text()) == 1:
                                self.main_product_details_panel_units_box_unit.setText('UNIDADE')
                            else:
                                self.main_product_details_panel_units_box_unit.setText('UNIDADES')
                            self.main_product_details_panel_price.setText(u'R$ ' + str("%.2f" % float(float(self.main_product_code_number.text().replace(',', '.')) * int(self.main_product_details_panel_units_box_number.text()))).replace('.', ','))

        if event.key() == QtCore.Qt.Key_F2:
            if not self.bottom_panel_number.echoMode() == 2:
                self.bottom_panel_title.setText(u"Digite a senha do administrador:")
                self.bottom_panel_title.setStyleSheet("QWidget { color: rgb(255,0,0) }")
                self.bottom_panel_number.setText('')
                self.bottom_panel_number.setEchoMode(2)
                self.bottom_panel_number.setStyleSheet("QWidget { border : none ; background: rgba(255,0,0,20%) ; border-radius: 8px;}")

    def voice_change_option(self):
        if self.voice:
            self.voice = False;
            self.top_panel_sound_icon.setShown(False)
        else:
            self.voice = True;
            if self.voice:
                subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', 'Auxílio sonoro.'])
            self.top_panel_sound_icon.setShown(True)

    def confirm_price(self):
        if not self.bottom_panel.y() == 0:
            if self.bottom_panel.y() == QtGui.QDesktopWidget().screenGeometry().height() and self.main_product_code_panel_transparency.opacity() == 0:
                self.main_product_code_title.setText(u"<strong>Qual é o código do próximo produto?</strong><br><small>Se não há código de barras, veja a lista no quadro.</small>")
                if self.voice:
                    subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', 'Sim. Produto adicionado. Qual é o código do próximo produto?'])
                self.add_product()
                self.show_product_code_panel()
            elif self.bottom_panel_yes.isVisible() == True and not self.bottom_panel.y() == QtGui.QDesktopWidget().screenGeometry().height():
                self.left_panel_table_01_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel_table_02_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel_table_03_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel_table_04_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel_table_05_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel_table_06_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel_table_07_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel_table_08_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel_table_09_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel_table_10_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")
                self.left_panel.setShown(True)
                self.main_product_code_panel.setShown(True)
                self.main_panel_donate_tip.setShown(False)
                self.parcial_price = 0.0
                self.show_history()

                self.top_panel_animation.setDuration(1000)
                self.top_panel_animation.setStartValue(QtCore.QRect(self.top_panel.x(),self.top_panel.y(),self.top_panel.width(),self.top_panel.height()))
                self.top_panel_animation.setEndValue(QtCore.QRect(0, int(int(self.top_panel.height() - 150) * - 1), QtGui.QDesktopWidget().screenGeometry().width(), int(QtGui.QDesktopWidget().screenGeometry().height() * 0.6)))
                self.top_panel_animation.start()

                self.bottom_panel_animation.setDuration(1000)
                self.bottom_panel_animation.setStartValue(QtCore.QRect(self.bottom_panel.x(),self.bottom_panel.y(),self.bottom_panel.width(),self.bottom_panel.height()))
                self.bottom_panel_animation.setEndValue(QtCore.QRect(0, QtGui.QDesktopWidget().screenGeometry().height(),self.bottom_panel.width(),self.bottom_panel.height()))
                self.bottom_panel_animation.start()

                self.main_product_code_title.setText(u"<strong>Qual é o código do produto?</strong><br><small>Se não há código de barras, veja a lista no quadro.</small>")

                if self.voice:
                    subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', str(self.left_panel_title.text()) + " Qual é o código do produto?"])

                self.show_product_code_panel()

            if self.main_panel_donate_tip.isVisible():
                self.clean_screen_to_welcome_screen()
                self.parcial_price = 0.0

    def negate_price(self):
        if self.voice:
            subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', 'Não'])
        if not self.bottom_panel.y() == 0:
            if self.bottom_panel.y() == QtGui.QDesktopWidget().screenGeometry().height() and self.main_product_code_panel_transparency.opacity() == 0:
                self.show_product_code_panel()
                self.main_product_code_title.setText(u"<strong>Qual é o código do próximo produto?</strong><br><small>Se não há código de barras, veja a lista no quadro.</small>")
            elif self.bottom_panel_yes.isVisible() == True or (self.bottom_panel.y() == QtGui.QDesktopWidget().screenGeometry().height() and self.main_product_code_panel_transparency.opacity() == 1):
                self.clean_screen_to_welcome_screen()
                self.parcial_price = 0.0

                if len(os.listdir('/media/')) >= 1:
                    self.generate_backup_package('/media/' + os.listdir('/media/')[0] + '/' + 'lojinha-backup-' + datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d") + datetime.datetime.now().strftime("%H") + datetime.datetime.now().strftime("%M"))

    def clean_screen_to_welcome_screen(self):
        if self.voice:
            self.voice = False;
            self.top_panel_sound_icon.setShown(False)
        self.bottom_panel_yes.setShown(False)
        self.bottom_panel_no.setShown(False)
        self.bottom_panel_number.setShown(True)
        self.bottom_panel_title.setText(u"Qual é o seu número?")
        self.bottom_panel_number.setText('')
        self.open_start_screen()
        self.bottom_panel_number.setFocus(1)
        self.bottom_panel_number.setEchoMode(0)
        self.bottom_panel_title.setStyleSheet("QWidget { }")
        self.bottom_panel_number.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,20%) ; border-radius: 8px;}")
        self.setCursor(QtGui.QCursor(10))

    def configuration_panel_members_change_number(self):
        profile = ConfigParser.RawConfigParser()
        profile.read(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_members_number.value()))))
        if os.path.isfile(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_members_number.value())))) and not unicode(profile.get('geral', 'nome'), 'utf-8') == self.configuration_panel_members_name_field.text():
            self.configuration_panel_members_number_warning.setText(u"Esse é o número de " + unicode(profile.get('geral', 'nome'), 'utf-8') + '.')
            self.configuration_panel_members_confirm_add_button.setEnabled(False)
            self.configuration_panel_members_confirm_edit_button.setEnabled(False)
            self.configuration_panel_members_number_warning.setShown(True)
        else:
            self.configuration_panel_members_confirm_add_button.setEnabled(True)
            self.configuration_panel_members_confirm_edit_button.setEnabled(True)
            self.configuration_panel_members_number_warning.setShown(False)

    def configuration_panel_members_edit(self):
        profile = ConfigParser.RawConfigParser()
        profile.read(os.path.join(dirpathNames, str(self.configuration_panel_members_list.currentItem().text().split(' - ')[0])))
        self.configuration_panel_members_name_field.setText(unicode(profile.get('geral', 'nome'), 'utf-8'))
        self.configuration_panel_members_number.setValue(int(str(self.configuration_panel_members_list.currentItem().text().split(' - ')[0])))

        self.configuration_panel_members_list.setEnabled(False)
        self.configuration_panel_members_name_title.setShown(True)
        self.configuration_panel_members_name_field.setShown(True)
        self.configuration_panel_members_number_title.setShown(True)
        self.configuration_panel_members_number.setShown(True)
        self.configuration_panel_members_confirm_edit_button.setShown(True)
        self.configuration_panel_members_confirm_edit_button.setEnabled(True)
        self.configuration_panel_members_confirm_add_button.setShown(False)
        self.configuration_panel_members_edit_button.setEnabled(False)
        self.configuration_panel_members_add_button.setEnabled(False)
        self.configuration_panel_members_remove_button.setEnabled(False)
        self.configuration_panel_members_cancel_button.setShown(True)

    def configuration_panel_members_add(self):
        self.configuration_panel_members_list.setEnabled(False)
        self.configuration_panel_members_name_field.setText('')
        self.configuration_panel_members_name_title.setShown(True)
        self.configuration_panel_members_name_field.setShown(True)
        self.configuration_panel_members_number_title.setShown(True)
        self.configuration_panel_members_number.setShown(True)
        self.configuration_panel_members_number.setValue(0)
        self.configuration_panel_members_confirm_edit_button.setShown(False)
        self.configuration_panel_members_confirm_add_button.setShown(True)
        self.configuration_panel_members_confirm_add_button.setEnabled(True)
        self.configuration_panel_members_edit_button.setEnabled(False)
        self.configuration_panel_members_add_button.setEnabled(False)
        self.configuration_panel_members_remove_button.setEnabled(False)
        self.configuration_panel_members_cancel_button.setShown(True)

    def configuration_panel_members_remove(self):
        os.remove(os.path.join(dirpathNames, str(self.configuration_panel_members_list.currentItem().text().split(' - ')[0])))
        self.feed_list_of_members()
        self.configuration_panel_members_edit_button.setEnabled(False)
        self.configuration_panel_members_remove_button.setEnabled(False)

    def configuration_panel_members_confirm_edit(self):
        profile = ConfigParser.RawConfigParser()
        profile.read(os.path.join(dirpathNames, str(self.configuration_panel_members_list.currentItem().text().split(' - ')[0])))
        profile.set('geral', 'nome', self.configuration_panel_members_name_field.text())
        profile.write(open(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_members_number.value()))), 'wb'))

        if not str(self.configuration_panel_members_list.currentItem().text().split(' - ')[0]) == str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_members_number.value())):
            os.remove(os.path.join(dirpathNames, str(self.configuration_panel_members_list.currentItem().text().split(' - ')[0])))

        self.configuration_panel_members_cancel()

    def configuration_panel_members_confirm_add(self):
        profile = ConfigParser.RawConfigParser()
        profile.add_section('geral')
        profile.set('geral', 'nome', self.configuration_panel_members_name_field.text())
        profile.set('geral', 'ultimo-balanco', datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d") + datetime.datetime.now().strftime("%H") + datetime.datetime.now().strftime("%M"))
        profile.add_section('produtos')
        profile.set('produtos', 'lista', '')
        profile.write(open(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_members_number.value()))), 'wb'))

        self.configuration_panel_members_cancel()

    def configuration_panel_members_cancel(self):
        self.configuration_panel_members_list.setEnabled(True)
        self.configuration_panel_members_name_title.setShown(False)
        self.configuration_panel_members_name_field.setShown(False)
        self.configuration_panel_members_number_title.setShown(False)
        self.configuration_panel_members_number.setShown(False)
        self.configuration_panel_members_cancel_button.setShown(False)
        self.configuration_panel_members_number_warning.setShown(False)
        self.configuration_panel_members_confirm_add_button.setShown(False)
        self.configuration_panel_members_confirm_edit_button.setShown(False)
        self.feed_list_of_members()

    def configutation_panel_members_list_change(self):
        self.configuration_panel_members_edit_button.setEnabled(True)
        self.configuration_panel_members_remove_button.setEnabled(True)

    def feed_list_of_members(self):
        self.configuration_panel_members_list.clear()

        del list_of_members[:]

        for i in range(len(os.listdir(os.path.join(dirpathConfig, 'irmaos')))):
            item = str(os.listdir(os.path.join(dirpathConfig, 'irmaos'))[i])

            if not item == str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo'))):
                profile = ConfigParser.RawConfigParser()
                profile.read(os.path.join(dirpathNames, item))
                item += ' - ' + unicode(profile.get('geral', 'nome'), 'utf-8')

                list_of_members.append(item)

        list_of_members.sort()

        self.configuration_panel_members_list.addItems(list_of_members)

        self.configuration_panel_members_edit_button.setEnabled(False)
        self.configuration_panel_members_add_button.setEnabled(True)
        self.configuration_panel_members_remove_button.setEnabled(False)

    def configuration_panel_products_code_field_change(self):
        self.configuration_panel_products_confirm_edit_button.setEnabled(True)
        self.configuration_panel_products_confirm_add_button.setEnabled(True)
        if self.configuration_panel_products_code_field.text() == '':
            self.configuration_panel_products_confirm_edit_button.setEnabled(False)
            self.configuration_panel_products_confirm_add_button.setEnabled(False)
        elif len(self.configuration_panel_products_code_field.text()) < 8:
            number = self.configuration_panel_products_code_field.text()
            self.configuration_panel_products_code_field.setText(str("%08d" % int(number)))

    def configuration_panel_products_change_price(self):
        original_price = self.configuration_panel_products_price.text()
        if not original_price == '':
            self.configuration_panel_products_price.setText(str("%.2f" % float(original_price.replace(",", "."))).replace('.', ','))
        else:
            self.configuration_panel_products_price.setText('0,00')

    def configuration_panel_products_edit(self):
        product_profile = ConfigParser.RawConfigParser()
        product_profile.read(os.path.join(dirpathProducts, str(self.configuration_panel_products_list.currentItem().text().split(' - ')[0])))

        self.configuration_panel_products_list.setEnabled(False)

        self.configuration_panel_products_code_title.setShown(True)
        self.configuration_panel_products_code_field.setShown(True)
        self.configuration_panel_products_code_field.setText(str(self.configuration_panel_products_list.currentItem().text().split(' - ')[0]))
        self.configuration_panel_products_name_title.setShown(True)
        self.configuration_panel_products_name_field.setShown(True)
        self.configuration_panel_products_name_field.setText(unicode(product_profile.get('geral', 'nome'), 'utf-8'))
        self.configuration_panel_products_description_title.setShown(True)
        self.configuration_panel_products_description_field.setShown(True)
        self.configuration_panel_products_description_field.setText(unicode(product_profile.get('geral', 'descricao'), 'utf-8'))
        self.configuration_panel_products_stock_title.setShown(True)
        self.configuration_panel_products_stock.setShown(True)
        self.configuration_panel_products_stock.setValue(int(product_profile.get('geral', 'estoque')))
        self.configuration_panel_products_price_title.setShown(True)
        self.configuration_panel_products_price_title_rs.setShown(True)
        self.configuration_panel_products_price.setShown(True)
        self.configuration_panel_products_price.setText(unicode(product_profile.get('geral', 'preco'), 'utf-8'))
        self.configuration_panel_products_variableprice.setChecked(product_profile.getboolean('geral', 'valor-variavel'))
        if product_profile.getboolean('geral', 'valor-variavel'):
            self.configuration_panel_products_stock_title.setEnabled(False)
            self.configuration_panel_products_stock.setEnabled(False)
        else:
            self.configuration_panel_products_stock_title.setEnabled(True)
            self.configuration_panel_products_stock.setEnabled(True)
        self.configuration_panel_products_variableprice.setShown(True)
        self.configuration_panel_products_cancel_button.setShown(True)

        self.configuration_panel_products_confirm_edit_button.setShown(True)
        self.configuration_panel_products_confirm_edit_button.setEnabled(True)
        self.configuration_panel_products_edit_button.setEnabled(False)
        self.configuration_panel_products_add_button.setEnabled(False)
        self.configuration_panel_products_remove_button.setEnabled(False)

    def configuration_panel_products_add(self):
        self.configuration_panel_products_list.setEnabled(False)

        self.configuration_panel_products_code_title.setShown(True)
        self.configuration_panel_products_code_field.setShown(True)
        self.configuration_panel_products_code_field.setText('')
        self.configuration_panel_products_name_title.setShown(True)
        self.configuration_panel_products_name_field.setShown(True)
        self.configuration_panel_products_name_field.setText('')
        self.configuration_panel_products_description_title.setShown(True)
        self.configuration_panel_products_description_field.setShown(True)
        self.configuration_panel_products_description_field.setText('')
        self.configuration_panel_products_stock_title.setShown(True)
        self.configuration_panel_products_stock_title.setEnabled(True)
        self.configuration_panel_products_stock.setShown(True)
        self.configuration_panel_products_stock.setEnabled(True)
        self.configuration_panel_products_stock.setValue(1)
        self.configuration_panel_products_price_title.setShown(True)
        self.configuration_panel_products_price_title_rs.setShown(True)
        self.configuration_panel_products_price.setShown(True)
        self.configuration_panel_products_price.setText('0,00')
        self.configuration_panel_products_variableprice.setChecked(False)
        self.configuration_panel_products_variableprice.setShown(True)
        self.configuration_panel_products_code_field.setFocus(1)
        self.configuration_panel_products_cancel_button.setShown(True)

        self.configuration_panel_products_confirm_add_button.setShown(True)
        self.configuration_panel_products_confirm_add_button.setEnabled(False)
        self.configuration_panel_products_edit_button.setEnabled(False)
        self.configuration_panel_products_add_button.setEnabled(False)
        self.configuration_panel_products_remove_button.setEnabled(False)

    def configuration_panel_products_remove(self):
        os.remove(os.path.join(dirpathProducts, str(self.configuration_panel_products_list.currentItem().text().split(' - ')[0])))
        self.feed_list_of_products()

    def configuration_panel_products_confirm_edit(self):
        product_profile = ConfigParser.RawConfigParser()
        product_profile.add_section('geral')
        product_profile.set('geral', 'nome', self.configuration_panel_products_name_field.text())
        product_profile.set('geral', 'descricao', self.configuration_panel_products_description_field.text())
        product_profile.set('geral', 'estoque', self.configuration_panel_products_stock.value())
        product_profile.set('geral', 'preco', self.configuration_panel_products_price.text())
        product_profile.set('geral', 'valor-variavel', self.configuration_panel_products_variableprice.isChecked())
        product_profile.write(open(os.path.join(dirpathProducts, str(self.configuration_panel_products_code_field.text())), 'wb'))

        if not str(self.configuration_panel_products_list.currentItem().text().split(' - ')[0]) == str(self.configuration_panel_products_code_field.text()):# or not str(self.configuration_panel_products_list.currentItem().text().split(' - ')[0]) == str("%08d" % int(self.configuration_panel_products_code_field.text())):
            os.remove(os.path.join(dirpathProducts, str(self.configuration_panel_products_list.currentItem().text().split(' - ')[0])))

        self.configuration_panel_products_cancel()

        self.configuration_panel_products_confirm_edit_button.setShown(False)


    def configuration_panel_products_confirm_add(self):
        product_profile = ConfigParser.RawConfigParser()
        product_profile.add_section('geral')
        product_profile.set('geral', 'nome', self.configuration_panel_products_name_field.text())
        product_profile.set('geral', 'descricao', self.configuration_panel_products_description_field.text())
        product_profile.set('geral', 'estoque', self.configuration_panel_products_stock.value())
        product_profile.set('geral', 'preco', self.configuration_panel_products_price.text())
        product_profile.set('geral', 'valor-variavel', self.configuration_panel_products_variableprice.isChecked())
        product_profile.write(open(os.path.join(dirpathProducts, str(self.configuration_panel_products_code_field.text())), 'wb'))

        self.configuration_panel_products_cancel()

        self.configuration_panel_products_confirm_add_button.setShown(False)


    def configuration_panel_products_cancel(self):
        self.configuration_panel_products_list.setEnabled(True)
        self.configuration_panel_products_code_title.setShown(False)
        self.configuration_panel_products_code_field.setShown(False)
        self.configuration_panel_products_name_title.setShown(False)
        self.configuration_panel_products_name_field.setShown(False)
        self.configuration_panel_products_description_title.setShown(False)
        self.configuration_panel_products_description_field.setShown(False)
        self.configuration_panel_products_stock_title.setShown(False)
        self.configuration_panel_products_stock.setShown(False)
        self.configuration_panel_products_price_title.setShown(False)
        self.configuration_panel_products_price_title_rs.setShown(False)
        self.configuration_panel_products_price.setShown(False)
        self.configuration_panel_products_variableprice.setShown(False)
        self.configuration_panel_products_cancel_button.setShown(False)
        self.configuration_panel_products_confirm_add_button.setShown(False)
        self.configuration_panel_products_confirm_edit_button.setShown(False)
        self.feed_list_of_products()

    def configutation_panel_products_list_change(self):
        self.configuration_panel_products_edit_button.setEnabled(True)
        self.configuration_panel_products_remove_button.setEnabled(True)

    def feed_list_of_products(self):
        self.configuration_panel_products_list.clear()

        del list_of_products[:]

        for i in range(len(os.listdir(os.path.join(dirpathConfig, 'produtos')))):
            item = str(os.listdir(os.path.join(dirpathConfig, 'produtos'))[i])

            product_profile = ConfigParser.RawConfigParser()
            product_profile.read(os.path.join(dirpathProducts, item))
            item += ' - ' + unicode(product_profile.get('geral', 'nome'), 'utf-8')

            list_of_products.append(item)

        list_of_products.sort()

        self.configuration_panel_products_list.addItems(list_of_products)

        self.configuration_panel_products_edit_button.setEnabled(False)
        self.configuration_panel_products_add_button.setEnabled(True)
        self.configuration_panel_products_remove_button.setEnabled(False)

    def show_configuration_panel(self):
        if self.bottom_panel.y() == int(QtGui.QDesktopWidget().screenGeometry().height() * 0.4):
            self.configuration_panel.setShown(True)
            self.configuration_panel_transparency_animation.setDuration(1000)
            self.configuration_panel_transparency_animation.setStartValue(self.configuration_panel_transparency.opacity())
            self.configuration_panel_transparency_animation.setEndValue(1.0)
            self.configuration_panel_transparency_animation.start()

            self.bottom_number_panel_transparency_animation.setDuration(1000)
            self.bottom_number_panel_transparency_animation.setStartValue(self.bottom_number_panel_transparency.opacity())
            self.bottom_number_panel_transparency_animation.setEndValue(0.0)
            self.bottom_number_panel_transparency_animation.start()

            self.bottom_panel_animation.setDuration(1000)
            self.bottom_panel_animation.setStartValue(QtCore.QRect(self.bottom_panel.x(),self.bottom_panel.y(),self.bottom_panel.width(),self.bottom_panel.height()))
            self.bottom_panel_animation.setEndValue(QtCore.QRect(0, 0, self.bottom_panel.width(), self.bottom_panel.height()))
            self.bottom_panel_animation.start()

            self.top_panel_animation.setDuration(1000)
            self.top_panel_animation.setStartValue(QtCore.QRect(self.top_panel.x(),self.top_panel.y(),self.top_panel.width(),self.top_panel.height()))
            self.top_panel_animation.setEndValue(QtCore.QRect(0, int(int(self.top_panel.height()) * - 1), QtGui.QDesktopWidget().screenGeometry().width(), int(QtGui.QDesktopWidget().screenGeometry().height() * 0.6)))
            self.top_panel_animation.start()

            self.setCursor(QtGui.QCursor(0))

        else:
            self.open_start_screen()
            self.setCursor(QtGui.QCursor(10))

    def add_product(self):
        profile = ConfigParser.RawConfigParser()
        profile.read(os.path.join(dirpathNames, str(self.bottom_panel_number.text())))

        self.main_product_code_number.setEnabled(True)

        final_price = 0.00

        if os.path.isfile(os.path.join(dirpathProducts, str(self.main_product_code_number.text()))):
            code = str(self.main_product_code_number.text())
            product_profile = ConfigParser.RawConfigParser()
            product_profile.read(os.path.join(dirpathProducts, str(self.main_product_code_number.text())))
            actual_stock = int(product_profile.get('geral', 'estoque'))
            actual_price = float(product_profile.get('geral', 'preco').replace(',', '.'))
            if product_profile.getboolean('geral', 'valor-variavel'):
                final_price = float(str(self.main_product_details_panel_price_field.text())[3:].replace(',', '.'))
                self.main_product_details_panel_units_box_number.setText('1')
                product_profile.set('geral', 'preco', str("%0.2f" % float(float(str(self.main_product_details_panel_price_field.text())[3:].replace(',', '.')) - actual_price)))
            else:
                final_price = float(str(self.main_product_details_panel_price.text())[3:].replace(',', '.'))
                if not actual_stock == 0:
                    product_profile.set('geral', 'estoque', str(actual_stock - int(self.main_product_details_panel_units_box_number.text())))
            product_profile.write(open(os.path.join(dirpathProducts, str(self.main_product_code_number.text())), 'wb'))
        else:
            code = 'XXXXXXXXXXXXX'

        if profile.get('produtos', 'lista') == '':
            list = ''
        else:
            list = profile.get('produtos', 'lista') + ','

        profile.set('produtos', 'lista', list + str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d") + datetime.datetime.now().strftime("%H") + datetime.datetime.now().strftime("%M") + '-' + code + '(' + str(str(final_price) + '-' + str(self.main_product_details_panel_units_box_number.text()) + ')')))

        profile.write(open(os.path.join(dirpathNames, str(self.bottom_panel_number.text())), 'wb'))

        self.parcial_price += final_price

        self.show_history()

        self.left_panel_table_01_transparency_animation.setDuration(2000)
        self.left_panel_table_01_transparency_animation.setStartValue(0.0)
        self.left_panel_table_01_transparency_animation.setEndValue(1.0)
        self.left_panel_table_01_transparency_animation.start()

        self.left_panel_table_01_geometry_animation.setDuration(2000)
        self.left_panel_table_01_geometry_animation.setStartValue(QtCore.QRect(self.left_panel_table_01.width(), self.left_panel_table_01.y(), self.left_panel_table_01.width(), self.left_panel_table_01.height()))
        self.left_panel_table_01_geometry_animation.setEndValue(QtCore.QRect(int(self.left_panel.width() * 0.1), self.left_panel_table_01.y(), self.left_panel_table_01.width(), self.left_panel_table_01.height()))
        self.left_panel_table_01_geometry_animation.start()

        self.left_panel_table_01_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")

    def open_start_screen(self):
        self.bottom_panel_title.setText(u"Qual é o seu número?")

        self.top_panel_animation.setDuration(1000)
        self.top_panel_animation.setStartValue(QtCore.QRect(self.top_panel.x(),self.top_panel.y(),self.top_panel.width(),self.top_panel.height()))
        self.top_panel_animation.setEndValue(QtCore.QRect(self.top_panel.x(), 0, self.top_panel.width(), self.top_panel.height()))
        self.top_panel_animation.start()

        self.bottom_panel_animation.setDuration(1000)
        self.bottom_panel_animation.setStartValue(QtCore.QRect(self.bottom_panel.x(),self.bottom_panel.y(),self.bottom_panel.width(),self.bottom_panel.height()))
        self.bottom_panel_animation.setEndValue(QtCore.QRect(0, int(QtGui.QDesktopWidget().screenGeometry().height() * 0.4), self.bottom_panel.width(),self.bottom_panel.height()))
        self.bottom_panel_animation.start()

        self.configuration_panel_transparency_animation.setDuration(1000)
        self.configuration_panel_transparency_animation.setStartValue(self.configuration_panel_transparency.opacity())
        self.configuration_panel_transparency_animation.setEndValue(0.0)
        self.configuration_panel_transparency_animation.start()

        self.bottom_number_panel_transparency_animation.setDuration(1000)
        self.bottom_number_panel_transparency_animation.setStartValue(self.bottom_number_panel_transparency.opacity())
        self.bottom_number_panel_transparency_animation.setEndValue(1.0)
        self.bottom_number_panel_transparency_animation.start()

        self.show_product_code_panel()

        self.bottom_panel_number.setText('')
        self.bottom_panel_number.setFocus(1)

    def change_product_number(self):
        if ',' in self.main_product_code_number.text():
            self.voice_change_option()
            self.main_product_code_number.setText('')

        if '/' in str(self.main_product_code_number.text()) or '*' in str(self.main_product_code_number.text()) or ' ' in str(self.main_product_code_number.text()):
            old_value = str(self.main_product_code_number.text())
            if ' ' in str(self.main_product_code_number.text()) or (len(old_value) == 1 and '*' in str(self.main_product_code_number.text())):
                if self.voice:
                    text = 'Não.'
                if str(self.bottom_panel_number.text()) == str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo'))) and not self.parcial_price == 0.0:
                    self.left_panel.setShown(False)
                    self.main_product_code_panel.setShown(False)
                    self.main_panel_donate_tip.setShown(True)
                    if self.voice:
                        text += " Faça seu pagamento na caixa ao lado no valor de "

                        if not int(str(self.parcial_price).split('.')[0]) == 0:
                            if int(str(self.parcial_price).split('.')[0]) == 1:
                                text += 'um real e '
                            else:
                                text += str(int(str(self.parcial_price).split('.')[0])) + ' reais e '
                        if not int(str(self.parcial_price).split('.')[1]) == 0:
                            if int(str(self.parcial_price).split('.')[1]) == 1:
                                text += ' um centavo.'
                            else:
                                text += str(int(str(self.parcial_price).split('.')[1])) + ' centavos.'
                        text += " Valores adicionais serão colocados como donativo para a construção do Salão de Assembleias."
                        subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', text])
                    self.bottom_panel_number.setText('')
                    self.main_product_code_number.setText('')
                    self.setFocus(1)
                    if self.voice:
                        text += " Qual é o código do produto?"
                else:
                    if self.voice:
                        text += " Finalizado."
                        if not self.parcial_price == 0.0:
                            text += " Total dos itens: "

                            if not int(str(self.parcial_price).split('.')[0]) == 0:
                                if int(str(self.parcial_price).split('.')[0]) == 1:
                                    text += 'um real e '
                                else:
                                    text += str(int(str(self.parcial_price).split('.')[0])) + ' reais e '
                            if not int(str(self.parcial_price).split('.')[1]) == 0:
                                if int(str(self.parcial_price).split('.')[1]) == 1:
                                    text += ' um centavo.'
                                else:
                                    text += str(int(str(self.parcial_price).split('.')[1])) + ' centavos.'
                        subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', text])
                    self.clean_screen_to_welcome_screen()
            elif len(old_value) > 1 and '*' in str(self.main_product_code_number.text()):
                self.main_product_code_number.setText('')
            elif '/' in str(self.main_product_code_number.text()):
                self.main_product_code_number.setText(old_value.replace('/', ''))
                self.change_product_number_finished()
            else:
                self.change_product_number_finished()

        if len(str(self.main_product_code_number.text())) > 13:
            self.main_product_code_number.setText('')
            self.main_product_code_title.setText(u"<strong>Tem certeza que esse é o código do produto?</strong><br><small>Você pode utilizar o leitor de código de barras.</small>")
            if self.voice:
                subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', "Tem certeza que esse é o código do produto? Você pode utilizar o leitor de código de barras."])
        #else:
            #self.main_product_code_title.setText(u"<strong>Qual é o código do produto?</strong><br><small>Se não há código de barras, veja a lista no quadro.</small>")


    def change_product_number_finished(self):
        if not self.main_product_code_number.text().replace('/', '').replace('*', '') == '':
            if len(str(self.main_product_code_number.text().replace('/', '').replace('*', ''))) < 8:
                self.main_product_code_number.setText(str("%08d" % int(self.main_product_code_number.text().replace('/', '').replace('*', ''))))

            if len(str(self.main_product_code_number.text().replace('/', '').replace('*', ''))) <= 13 and len(str(self.main_product_code_number.text().replace('/', '').replace('*', ''))) >= 8:
                if os.path.isfile(os.path.join(dirpathProducts, str(self.main_product_code_number.text().replace('/', '').replace('*', '')))):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, str(self.main_product_code_number.text().replace('/', '').replace('*', ''))))

                    self.main_product_details_panel_title.setText('<strong><big>' + unicode(product_profile.get('geral', 'nome'), 'utf-8') + '</strong></big><br></br><small>' + unicode(product_profile.get('geral', 'descricao'), 'utf-8') + '</small>')
                    if product_profile.getboolean('geral', 'valor-variavel'):
                        self.main_product_details_panel_units_box.setShown(False)
                        self.main_product_details_panel_price.setShown(False)
                        self.main_product_details_panel_price_field.setShown(True)
                        self.main_product_details_panel_price_field.setText('R$ 0,00')
                        self.main_product_details_panel_price_field.setFocus(1)
                        self.main_product_details_panel_price_field.deselect()
                        self.main_product_details_panel_price_field.setCursorPosition(len(self.main_product_details_panel_price_field.text()))
                    else:
                        self.main_product_details_panel_units_box.setShown(True)
                        self.main_product_details_panel_price.setShown(True)
                        self.main_product_details_panel_price_field.setShown(False)
                        self.main_product_details_panel_units_box_number.setText('1')
                        self.main_product_details_panel_units_box_unit.setText('UNIDADE')
                        self.main_product_details_panel_price.setText(u'R$ ' + unicode(product_profile.get('geral', 'preco'), 'utf-8'))
                        self.setFocus(1)

                    self.show_product_detail_panel()


                else:
                    print self.main_product_code_number.text()
                    self.main_product_code_number.setText('')
                    self.main_product_code_title.setText(u"<strong>Tem certeza que esse é o código do produto?</strong><br><small>Você pode utilizar o leitor de código de barras.</small>")
                    if self.voice:
                        subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', "Tem certeza que esse é o código do produto? Você pode utilizar o leitor de código de barras."])


    def show_product_code_panel(self):
        self.main_product_code_number.setEnabled(True)
        self.main_product_code_number.setFocus(1)
        self.main_product_code_number.setText('')

        #self.main_product_code_title.setText(u"<strong>Qual é o código do produto?</strong><br><small>Se não há código de barras, veja a lista no quadro.</small>")

        self.main_product_code_panel_animation.setDuration(1000)
        self.main_product_code_panel_animation.setStartValue(QtCore.QRect(self.main_product_code_panel.x(),self.main_product_code_panel.y(),self.main_product_code_panel.width(),self.main_product_code_panel.height()))
        self.main_product_code_panel_animation.setEndValue(QtCore.QRect(int(QtGui.QDesktopWidget().screenGeometry().width() * 0.33) + 50,self.main_product_code_panel.y(),self.main_product_code_panel.width(),self.main_product_code_panel.height()))
        self.main_product_code_panel_animation.start()

        self.main_product_code_panel_animation_transparency.setDuration(1000)
        self.main_product_code_panel_animation_transparency.setStartValue(self.main_product_code_panel_transparency.opacity())
        self.main_product_code_panel_animation_transparency.setEndValue(1.0)
        self.main_product_code_panel_animation_transparency.start()

        self.main_product_details_panel_animation.setDuration(1000)
        self.main_product_details_panel_animation.setStartValue(self.main_product_details_panel_transparency.opacity())
        self.main_product_details_panel_animation.setEndValue(0.0)
        self.main_product_details_panel_animation.start()

    def show_product_detail_panel(self):
        if self.voice and self.main_product_code_number.isEnabled():
            text = ''
            if len(str(int(self.main_product_code_number.text()))) <= 4:
                text += str(int(self.main_product_code_number.text())) + '.'
            text += self.main_product_details_panel_title.text().split('<big>')[1].split('</strong>')[0] + ' no valor de '
            if not int(self.main_product_details_panel_price.text().split(' ')[1].split(',')[0]) == 0:
                if int(self.main_product_details_panel_price.text().split(' ')[1].split(',')[0]) == 1:
                    text += 'um real e '
                else:
                    text += self.main_product_details_panel_price.text().split(' ')[1].split(',')[0] + ' reais e '
            if not int(self.main_product_details_panel_price.text().split(' ')[1].split(',')[1]) == 0:
                if int(self.main_product_details_panel_price.text().split(' ')[1].split(',')[1]) == 1:
                    text += ' um centavo.'
                else:
                    text += self.main_product_details_panel_price.text().split(' ')[1].split(',')[1] + ' centavos.'
            text += ' Voceh deseja adicionar esse item?'
            subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', text])

        self.main_product_code_number.setEnabled(False)

        self.main_product_code_panel_animation_transparency.setDuration(1000)
        self.main_product_code_panel_animation_transparency.setStartValue(self.main_product_code_panel_transparency.opacity())
        self.main_product_code_panel_animation_transparency.setEndValue(0.0)
        self.main_product_code_panel_animation_transparency.start()

        self.main_product_details_panel_animation.setDuration(1000)
        self.main_product_details_panel_animation.setStartValue(self.main_product_details_panel_transparency.opacity())
        self.main_product_details_panel_animation.setEndValue(1.0)
        self.main_product_details_panel_animation.start()

    def show_history(self):
        profile = ConfigParser.RawConfigParser()
        profile.read(os.path.join(dirpathNames, str(self.bottom_panel_number.text())))

        if int(datetime.datetime.now().strftime("%H")) < 12 and int(datetime.datetime.now().strftime("%H")) >= 5:
            welcomeText = (u"Bom dia")
        elif int(datetime.datetime.now().strftime("%H")) >= 12 and int(datetime.datetime.now().strftime("%H")) < 18:
            welcomeText = (u"Boa tarde")
        else:
            welcomeText = (u"Boa noite")

        if not str(self.bottom_panel_number.text()) == str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo'))):
            self.left_panel_title.setText(welcomeText + ' ' + unicode(profile.get('geral', 'nome').split(' ')[0], 'utf-8') + '.')
        else:
            self.left_panel_title.setText(welcomeText + '.')

        list_of_profile_products = profile.get('produtos', 'lista').split(',')

        if not profile.get('produtos', 'lista') == '' and not str(self.bottom_panel_number.text()) == str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo'))):
            if len(list_of_profile_products) >= 1:

                self.left_panel_table_01.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-1].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-1].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-1].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-1].split('-')[0][8:10] + ':' + list_of_profile_products[-1].split('-')[0][10:12]
                elif list_of_profile_products[-1].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-1].split('-')[0][8:10] + ':' + list_of_profile_products[-1].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-1].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-1].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-1].split('-')[0][8:10] + ':' + list_of_profile_products[-1].split('-')[0][10:12]

                self.left_panel_table_01_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-1].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_01_price.setText('R$ ' +  list_of_profile_products[-1].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_01.setShown(False)

            if len(list_of_profile_products) >= 2:

                self.left_panel_table_02.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-2].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-2].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-2].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-2].split('-')[0][8:10] + ':' + list_of_profile_products[-2].split('-')[0][10:12]
                elif list_of_profile_products[-2].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-2].split('-')[0][8:10] + ':' + list_of_profile_products[-2].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-2].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-2].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-2].split('-')[0][8:10] + ':' + list_of_profile_products[-2].split('-')[0][10:12]

                self.left_panel_table_02_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-2].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_02_price.setText('R$ ' +  list_of_profile_products[-2].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_02.setShown(False)

            if len(list_of_profile_products) >= 3:

                self.left_panel_table_03.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-3].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-3].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-3].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-3].split('-')[0][8:10] + ':' + list_of_profile_products[-3].split('-')[0][10:12]
                elif list_of_profile_products[-3].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-3].split('-')[0][8:10] + ':' + list_of_profile_products[-3].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-3].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-3].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-3].split('-')[0][8:10] + ':' + list_of_profile_products[-3].split('-')[0][10:12]

                self.left_panel_table_03_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-3].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_03_price.setText('R$ ' +  list_of_profile_products[-3].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_03.setShown(False)

            if len(list_of_profile_products) >= 4:

                self.left_panel_table_04.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-4].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-4].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-4].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-4].split('-')[0][8:10] + ':' + list_of_profile_products[-4].split('-')[0][10:12]
                elif list_of_profile_products[-4].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-4].split('-')[0][8:10] + ':' + list_of_profile_products[-4].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-4].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-4].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-4].split('-')[0][8:10] + ':' + list_of_profile_products[-4].split('-')[0][10:12]

                self.left_panel_table_04_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-4].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_04_price.setText('R$ ' +  list_of_profile_products[-4].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_04.setShown(False)

            if len(list_of_profile_products) >= 5:

                self.left_panel_table_05.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-5].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-5].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-5].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-5].split('-')[0][8:10] + ':' + list_of_profile_products[-5].split('-')[0][10:12]
                elif list_of_profile_products[-5].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-5].split('-')[0][8:10] + ':' + list_of_profile_products[-5].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-5].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-5].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-5].split('-')[0][8:10] + ':' + list_of_profile_products[-5].split('-')[0][10:12]

                self.left_panel_table_05_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-5].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_05_price.setText('R$ ' +  list_of_profile_products[-5].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_05.setShown(False)

            if len(list_of_profile_products) >= 6:

                self.left_panel_table_06.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-6].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-6].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-6].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-6].split('-')[0][8:10] + ':' + list_of_profile_products[-6].split('-')[0][10:12]
                elif list_of_profile_products[-6].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-6].split('-')[0][8:10] + ':' + list_of_profile_products[-6].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-6].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-6].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-6].split('-')[0][8:10] + ':' + list_of_profile_products[-6].split('-')[0][10:12]

                self.left_panel_table_06_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-6].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_06_price.setText('R$ ' +  list_of_profile_products[-6].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_06.setShown(False)

            if len(list_of_profile_products) >= 7:

                self.left_panel_table_07.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-7].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-7].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-7].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-7].split('-')[0][8:10] + ':' + list_of_profile_products[-7].split('-')[0][10:12]
                elif list_of_profile_products[-7].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-7].split('-')[0][8:10] + ':' + list_of_profile_products[-7].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-7].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-7].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-7].split('-')[0][8:10] + ':' + list_of_profile_products[-7].split('-')[0][10:12]

                self.left_panel_table_07_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-7].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_07_price.setText('R$ ' +  list_of_profile_products[-7].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_07.setShown(False)

            if len(list_of_profile_products) >= 8:

                self.left_panel_table_08.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-8].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-8].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-8].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-8].split('-')[0][8:10] + ':' + list_of_profile_products[-8].split('-')[0][10:12]
                elif list_of_profile_products[-8].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-8].split('-')[0][8:10] + ':' + list_of_profile_products[-8].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-8].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-8].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-8].split('-')[0][8:10] + ':' + list_of_profile_products[-8].split('-')[0][10:12]

                self.left_panel_table_08_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-8].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_08_price.setText('R$ ' +  list_of_profile_products[-8].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_08.setShown(False)

            if len(list_of_profile_products) >= 9:

                self.left_panel_table_09.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-9].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-9].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-9].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-9].split('-')[0][8:10] + ':' + list_of_profile_products[-9].split('-')[0][10:12]
                elif list_of_profile_products[-9].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-9].split('-')[0][8:10] + ':' + list_of_profile_products[-9].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-9].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-9].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-9].split('-')[0][8:10] + ':' + list_of_profile_products[-9].split('-')[0][10:12]

                self.left_panel_table_09_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-9].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_09_price.setText('R$ ' +  list_of_profile_products[-9].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_09.setShown(False)

            if len(list_of_profile_products) >= 10:

                self.left_panel_table_10.setShown(True)

                if os.path.isfile(os.path.join(dirpathProducts, list_of_profile_products[-10].split('-')[1].split('(')[0])):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, list_of_profile_products[-10].split('-')[1].split('(')[0]))
                    product_name = unicode(product_profile.get('geral', 'nome'), 'utf-8')
                else:
                    product_name = u'Produto sem código'

                if list_of_profile_products[-10].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d")):
                    product_date = 'Hoje, às ' + list_of_profile_products[-10].split('-')[0][8:10] + ':' + list_of_profile_products[-10].split('-')[0][10:12]
                elif list_of_profile_products[-10].split('-')[0][:8] == str(datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + str(int(datetime.datetime.now().strftime("%d")) - 1)):
                    product_date = 'Ontem, às ' + list_of_profile_products[-10].split('-')[0][8:10] + ':' + list_of_profile_products[-10].split('-')[0][10:12]
                else:
                    product_date = 'Dia ' + list_of_profile_products[-10].split('-')[0][6:8] + ' de ' + list_of_months[int(list_of_profile_products[-10].split('-')[0][4:6])] + ', às ' + list_of_profile_products[-10].split('-')[0][8:10] + ':' + list_of_profile_products[-10].split('-')[0][10:12]

                self.left_panel_table_10_title.setText(product_name + '<br><small>' + product_date + ' - ' + list_of_profile_products[-10].split('-')[2][:-1] + 'un.</small>')
                self.left_panel_table_10_price.setText('R$ ' +  list_of_profile_products[-10].split('-')[1].split('(')[1].split('-')[0].replace('.', ','))
            else:
                self.left_panel_table_10.setShown(False)

            if '246,210,154' in self.left_panel_table_09_border.styleSheet():
                self.left_panel_table_10_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")
            else:
                self.left_panel_table_10_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")

            if '246,210,154' in self.left_panel_table_08_border.styleSheet():
                self.left_panel_table_09_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")
            else:
                self.left_panel_table_09_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")

            if '246,210,154' in self.left_panel_table_07_border.styleSheet():
                self.left_panel_table_08_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")
            else:
                self.left_panel_table_08_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")

            if '246,210,154' in self.left_panel_table_06_border.styleSheet():
                self.left_panel_table_07_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")
            else:
                self.left_panel_table_07_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")

            if '246,210,154' in self.left_panel_table_05_border.styleSheet():
                self.left_panel_table_06_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")
            else:
                self.left_panel_table_06_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")

            if '246,210,154' in self.left_panel_table_04_border.styleSheet():
                self.left_panel_table_05_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")
            else:
                self.left_panel_table_05_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")

            if '246,210,154' in self.left_panel_table_03_border.styleSheet():
                self.left_panel_table_04_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")
            else:
                self.left_panel_table_04_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")

            if '246,210,154' in self.left_panel_table_02_border.styleSheet():
                self.left_panel_table_03_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")
            else:
                self.left_panel_table_03_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")

            if '246,210,154' in self.left_panel_table_01_border.styleSheet():
                self.left_panel_table_02_border.setStyleSheet("QWidget { border : none ; background: rgba(246,210,154,80%) ; border-radius: 8px;}")
            else:
                self.left_panel_table_02_border.setStyleSheet("QWidget { border : none ; background: rgba(255,255,255,80%) ; border-radius: 8px;}")


            total_price = 0.00
            if not list_of_profile_products[0] == '':
                for i in range(len(list_of_profile_products)):
                    if  int(list_of_profile_products[i].split('-')[0][:12]) >= int(profile.get('geral', 'ultimo-balanco')):
                        total_price += float(list_of_profile_products[i].split('-')[1].split('(')[1])

            self.left_panel_total_title2.setShown(True)
            self.left_panel_total_title2.setText("<strong>TOTAL GERAL</strong><br><small>DESDE " + profile.get('geral', 'ultimo-balanco')[6:8] + " DE " + list_of_months[int(profile.get('geral', 'ultimo-balanco')[4:6]) - 1].upper() + "</small>")
            self.left_panel_total_value2.setText(u"R$ " + str("%.2f" % total_price).replace('.', ','))

        elif str(self.bottom_panel_number.text()) == str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo'))):
            self.main_panel_donate_tip.setText(u"Faça seu pagamento na caixa ao lado no valor de R$ " + str("%.2f" % self.parcial_price).replace('.', ',') + ".<br><br><small>Valores adicionais serão colocados como donativo para a construção do Salão de Assembleias.")
            self.main_panel_donate_tip.setShown(False)
            self.left_panel_total_title2.setShown(False)
            self.left_panel_total_value2.setText(u"Pagamento")
            self.left_panel_table_01.setShown(False)
            self.left_panel_table_02.setShown(False)
            self.left_panel_table_03.setShown(False)
            self.left_panel_table_04.setShown(False)
            self.left_panel_table_05.setShown(False)
            self.left_panel_table_06.setShown(False)
            self.left_panel_table_07.setShown(False)
            self.left_panel_table_08.setShown(False)
            self.left_panel_table_09.setShown(False)
            self.left_panel_table_10.setShown(False)

        else:
            self.left_panel_table_01.setShown(False)
            self.left_panel_table_02.setShown(False)
            self.left_panel_table_03.setShown(False)
            self.left_panel_table_04.setShown(False)
            self.left_panel_table_05.setShown(False)
            self.left_panel_table_06.setShown(False)
            self.left_panel_table_07.setShown(False)
            self.left_panel_table_08.setShown(False)
            self.left_panel_table_09.setShown(False)
            self.left_panel_table_10.setShown(False)
            self.left_panel_total_value2.setText('')

        self.left_panel_total_value1.setText(u"R$ " + str("%.2f" % self.parcial_price).replace('.', ','))

    def change_number(self):
        if ',' in self.bottom_panel_number.text():
            self.voice_change_option()
            self.bottom_panel_number.setText('')
        elif '*' in self.bottom_panel_number.text() or ' ' in self.bottom_panel_number.text():
            self.bottom_panel_number.setText('')
        elif '/' in self.bottom_panel_number.text():
            old_number = str(self.bottom_panel_number.text()).replace('/', '')
            if not old_number == '':
                self.bottom_panel_number.setText(str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(old_number)))
            else:
                self.bottom_panel_number.setText('')
        self.configuration_panel.setShown(False)
        if self.bottom_panel_number.echoMode() == 2:
            if self.bottom_panel_number.text() == mainConfigFile.get('geral', 'senha'):
                self.show_configuration_panel()
        elif len(str(self.bottom_panel_number.text())) == int(mainConfigFile.get('geral', 'membro-digitos')):
            self.setFocus(1)
            if os.path.isfile(os.path.join(dirpathNames, str(self.bottom_panel_number.text()))):
                profile = ConfigParser.RawConfigParser()
                profile.read(os.path.join(dirpathNames,  str(self.bottom_panel_number.text())))

                if not str(self.bottom_panel_number.text()) == str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo'))):
                    self.bottom_panel_title.setText(u"Você é " + unicode(profile.get('geral', 'nome'), 'utf-8') + '?')
                    if self.voice and not self.bottom_panel_number.echoMode() == 2:
                        subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', "Você é " + unicode(profile.get('geral', 'nome'), 'utf-8') + '?'])
                else:
                    self.bottom_panel_title.setText(u"Você fará o pagamento na caixa ao lado neste momento?")
                    if self.voice and not self.bottom_panel_number.echoMode() == 2:
                        subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', "Você fará o pagamento na caixa ao lado neste momento?"])

                self.bottom_panel_yes.setShown(True)
                self.bottom_panel_no.setShown(True)
                self.bottom_panel_number.setShown(False)

            else:
                self.bottom_panel_title.setText(u"Número desconhecido. Qual é o seu número?")
                if self.voice and not self.bottom_panel_number.echoMode() == 2:
                    subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', "Número desconhecido. Qual é o seu número?"])
                self.bottom_panel_number.setText('')
                self.bottom_panel_number.setFocus(1)
        if self.voice and not self.bottom_panel_number.echoMode() == 2:
            if not len(str(self.bottom_panel_number.text())) == int(mainConfigFile.get('geral', 'membro-digitos')):
                subprocess.Popen(['espeak', '-v', 'mb-br4', '-s', '120', self.bottom_panel_number.text()[-1:]])

    def main_product_details_panel_price_field_changed(self):
        #if not self.main_product_details_panel_price_field.text() == '':
        if '/' in self.main_product_details_panel_price_field.text():
            self.main_product_details_panel_price_field.setText(self.main_product_details_panel_price_field.text().replace('R$ ', '').replace('/', ''))
            self.confirm_price()
        elif '*' in self.main_product_details_panel_price_field.text():
            self.main_product_details_panel_price_field.setText(self.main_product_details_panel_price_field.text().replace('R$ ', '').replace('*', ''))
            self.negate_price()

        old_value = str("%03d" % int(self.main_product_details_panel_price_field.text().replace('R$ ', '').replace(',', '')))
        if len(old_value) > 0 and len(old_value) <= 2:
            old_value = '0' + str(int(old_value))
        self.main_product_details_panel_price_field.setText('R$ ' + old_value[:-2] + ',' + old_value[-2:])
        self.main_product_details_panel_price_field.setCursorPosition(len(self.main_product_details_panel_price_field.text()))



    def configuration_panel_products_variableprice_clicked(self):
        if self.configuration_panel_products_variableprice.isChecked():
            self.configuration_panel_products_stock_title.setEnabled(False)
            self.configuration_panel_products_stock.setEnabled(False)
            self.configuration_panel_products_stock.setValue(0)
        else:
            self.configuration_panel_products_stock_title.setEnabled(True)
            self.configuration_panel_products_stock.setEnabled(True)

    def export_PDF(self):
        self.location_path = QtGui.QFileDialog.getExistingDirectory(self, "Selecione a pasta em que deseja salvar os relatórios", os.environ.get('HOME', None), QtGui.QFileDialog.ShowDirsOnly)

        if self.configuration_panel_balance_individual_panel_option_all.isChecked():
            list_of_members_for_report = list_of_members
        else:
            list_of_members_for_report = [str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_balance_individual_panel_option_one_name.value()))]

        for member in list_of_members_for_report:
            profile = ConfigParser.RawConfigParser()
            profile.read(os.path.join(dirpathNames, member.split(' - ')[0]))

            list_of_prices = []

            pdf = Canvas(self.location_path + '/' + member.split(' - ')[0] + ' - ' + unicode(profile.get('geral', 'nome'), 'utf-8') + '.pdf', pagesize = [210 * mm, 297 * mm])


            pdfmetrics.registerFont(TTFont('Ubuntu', 'Ubuntu-R.ttf'))
            pdfmetrics.registerFont(TTFont('Ubuntu-Bold', 'Ubuntu-B.ttf'))
            pdfmetrics.registerFont(TTFont('Ubuntu-Condensed', 'Ubuntu-C.ttf'))

            pdf.setFont("Ubuntu-Bold",16)
            pdf.drawString(float(20 * mm), float(297 * mm) - float(20 * mm), 'Relatório de produtos da Lojinha')
            pdf.setFont("Ubuntu",12)
            pdf.drawString(float(20 * mm), float(297 * mm) - float(25 * mm), profile.get('geral', 'nome') + unicode(' - Período de ', 'utf-8') + self.configuration_panel_balance_individual_panel_date_start.dateTime().toString("dd/MM/yyyy - hh:mm") + ' a ' + self.configuration_panel_balance_individual_panel_date_end.dateTime().toString("dd/MM/yyyy - hh:mm"))

            pdf.setFont("Ubuntu-Bold",8)
            pdf.drawString(float(20 * mm), float(297 * mm) - float(40 * mm), "Data")
            pdf.drawString(float(40 * mm), float(297 * mm) - float(40 * mm), "Código")
            pdf.drawString(float(60 * mm), float(297 * mm) - float(40 * mm), "Nome")
            pdf.drawString(float(140 * mm), float(297 * mm) - float(40 * mm), "Qtd.")
            pdf.drawRightString(float(170 * mm), float(297 * mm) - float(40 * mm), "Preço Unitário")
            pdf.drawRightString(float(190 * mm), float(297 * mm) - float(40 * mm), "Preço Total")

            line_distance = 50
            pdf.setFont("Ubuntu-Condensed",8)

            for product in profile.get('produtos', 'lista').split(','):
                if not product.split('-')[0][:12] == '' and int(product.split('-')[0][:12]) >= int(self.configuration_panel_balance_individual_panel_date_start.dateTime().toString("yyyyMMddhhmm")) and int(product.split('-')[0][:12]) <= int(self.configuration_panel_balance_individual_panel_date_end.dateTime().toString("yyyyMMddhhmm")):
                    pdf.drawString(float(20 * mm), float(297 * mm) - float(line_distance * mm), product.split('-')[0][6:8] + '/' + product.split('-')[0][4:6] + '/' + product.split('-')[0][:4] + ' - ' + product.split('-')[0][8:10] + ':' + product.split('-')[0][10:12])

                    pdf.drawString(float(40 * mm), float(297 * mm) - float(line_distance * mm), product.split('-')[1].split('(')[0])

                    if os.path.isfile(os.path.join(dirpathProducts, product.split('-')[1].split('(')[0])):
                        product_profile = ConfigParser.RawConfigParser()
                        product_profile.read(os.path.join(dirpathProducts, product.split('-')[1].split('(')[0]))
                        pdf.drawString(float(60 * mm), float(297 * mm) - float(line_distance * mm), product_profile.get('geral', 'nome'))
                    else:
                        pdf.drawString(float(60 * mm), float(297 * mm) - float(line_distance * mm), 'Sem nome')

                    pdf.drawString(float(140 * mm), float(297 * mm) - float(line_distance * mm), product.split('-')[2].split(')')[0])
                    pdf.drawRightString(float(170 * mm), float(297 * mm) - float(line_distance * mm), str("%.2f" % float(float(product.split('-')[1].split('(')[1].split('-')[0]) / int(product.split('-')[2].split(')')[0]))).replace('.', ','))
                    pdf.drawRightString(float(190 * mm), float(297 * mm) - float(line_distance * mm), str("%.2f" % float(product.split('-')[1].split('(')[1].split('-')[0])).replace('.', ','))

                    list_of_prices.append(float(product.split('-')[1].split('(')[1].split('-')[0]))

                    line_distance += 5

                    if line_distance >= 277:
                        pdf.showPage()
                        pdf.setFont("Ubuntu-Condensed",8)
                        line_distance = 20

            pdf.setFont("Ubuntu-Bold",8)
            pdf.drawString(float(150 * mm), float(297 * mm) - float(line_distance * mm), 'TOTAL GERAL:')
            pdf.drawRightString(float(190 * mm), float(297 * mm) - float(line_distance * mm), str("%.2f" % sum(list_of_prices)).replace('.', ','))

            pdf.showPage()
            pdf.save()

    def export_CSV(self):
        self.location_path = QtGui.QFileDialog.getExistingDirectory(self, "Selecione a pasta em que deseja salvar os relatórios", os.environ.get('HOME', None), QtGui.QFileDialog.ShowDirsOnly)

        csv_header = '"Data","Código","Nome","Quantidade","Preço Unitário","Preço Total"\n'

        if self.configuration_panel_balance_individual_panel_option_all.isChecked():
            list_of_members_for_report = list_of_members
        else:
            list_of_members_for_report = [str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_balance_individual_panel_option_one_name.value()))]

        for member in list_of_members_for_report:
            profile = ConfigParser.RawConfigParser()
            profile.read(os.path.join(dirpathNames, member.split(' - ')[0]))

            list_of_prices = []

            csv_final = ''
            csv_final += csv_header

            for product in profile.get('produtos', 'lista').split(','):
                if not product.split('-')[0][:12] == '' and int(product.split('-')[0][:12]) >= int(self.configuration_panel_balance_individual_panel_date_start.dateTime().toString("yyyyMMddhhmm")) and int(product.split('-')[0][:12]) <= int(self.configuration_panel_balance_individual_panel_date_end.dateTime().toString("yyyyMMddhhmm")):
                    product_profile = ConfigParser.RawConfigParser()
                    product_profile.read(os.path.join(dirpathProducts, product.split('-')[1].split('(')[0]))

                    csv_final += '"' + product.split('-')[0][6:8] + '/' + product.split('-')[0][4:6] + '/' + product.split('-')[0][:4] + ' - ' + product.split('-')[0][8:10] + ':' + product.split('-')[0][10:12] + '","'

                    if product.split('-')[1].split('(')[0][:1] == 'X':
                        csv_final += 'Produto sem código","'
                        csv_final += 'Produto sem nome","'
                    else:
                        csv_final += product.split('-')[1].split('(')[0] + '","'
                        csv_final += product_profile.get('geral', 'nome')  + '","'

                    csv_final += product.split('-')[2].split(')')[0]  + '","'
                    csv_final += str("%.2f" % float(float(product.split('-')[1].split('(')[1].split('-')[0]) / int(product.split('-')[2].split(')')[0]))).replace('.', ',')  + '","'
                    csv_final += str("%.2f" % float(product.split('-')[1].split('(')[1].split('-')[0])).replace('.', ',')  + '"\n'
                    list_of_prices.append(float(product.split('-')[1].split('(')[1].split('-')[0]))

            csv_final += '"","","","","",""\n"","","","","TOTAL GERAL:","' + str(sum(list_of_prices)).replace('.', ',') + '"\n'

            open(self.location_path + '/' + unicode(profile.get('geral', 'nome'), 'utf-8') + '.csv', 'w').write(csv_final)


    def close_balance(self):
        for member in list_of_members:
            profile = ConfigParser.RawConfigParser()
            profile.read(os.path.join(dirpathNames, member.split(' - ')[0]))
            profile.set('geral', 'ultimo-balanco', datetime.datetime.now().strftime("%Y") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime("%d") + datetime.datetime.now().strftime("%H") + datetime.datetime.now().strftime("%M"))
            profile.write(open(os.path.join(dirpathNames, member.split(' - ')[0]), 'wb'))

    def products_export_CSV(self):
        csv_final = '"Código","Nome","Descrição","Quantidade em estoque","Preço"\n'
        for item in list_of_products:
            product_profile = ConfigParser.RawConfigParser()
            product_profile.read(os.path.join(dirpathProducts, item.split(' - ')[0]))

            list_of_prices = []

            csv_final += '"' + item.split(' - ')[0] + '","'
            csv_final += product_profile.get('geral', 'nome') + '","'
            csv_final += product_profile.get('geral', 'descricao') + '","'
            csv_final += product_profile.get('geral', 'estoque') + '","'
            csv_final += product_profile.get('geral', 'preco') + '"\n'

            list_of_prices.append(float(product_profile.get('geral', 'preco')) * int(product_profile.get('geral', 'estoque')))

        csv_final += '"","","","","",""\n"","","","","TOTAL GERAL:","' + str(sum(list_of_prices)).replace('.', ',') + '"\n'

        self.location_path = QtGui.QFileDialog.getSaveFileName(self, u"Salvar relatório de produtos em CSV", os.environ.get('HOME', None), unicode(str(u"Tabela CSV (*.csv)"), 'utf-8')).toUtf8()
        open(self.location_path, 'w').write(csv_final)

    def products_export_PDF(self):
        self.location_path = QtGui.QFileDialog.getSaveFileName(self, u"Salvar relatório de produtos em PDF", os.environ.get('HOME', None), unicode(str(u"Documento em PDF (*.pdf)"), 'utf-8')).toUtf8()

        pdf = Canvas(self.location_path, pagesize = [210 * mm, 297 * mm])

        pdfmetrics.registerFont(TTFont('Ubuntu', 'Ubuntu-R.ttf'))
        pdfmetrics.registerFont(TTFont('Ubuntu-Bold', 'Ubuntu-B.ttf'))
        pdfmetrics.registerFont(TTFont('Ubuntu-Condensed', 'Ubuntu-C.ttf'))

        pdf.setFont("Ubuntu-Bold",16)
        pdf.drawString(float(20 * mm), float(297 * mm) - float(20 * mm), 'Relatório de produtos da Lojinha')
        pdf.setFont("Ubuntu",10)
        pdf.drawString(float(20 * mm), float(297 * mm) - float(25 * mm), 'Emitido no dia ' + datetime.datetime.now().strftime("%d") + '/' + datetime.datetime.now().strftime("%m") + '/' + datetime.datetime.now().strftime("%Y") + ', às ' + datetime.datetime.now().strftime("%H") + ':' + datetime.datetime.now().strftime("%M"))
        pdf.setFont("Ubuntu",8)
        pdf.drawString(float(20 * mm), float(297 * mm) - float(29 * mm), u'A soma dos itens que saíram abrange o período de ' + self.configuration_panel_balance_products_panel_date_start.dateTime().toString("dd/MM/yyyy - hh:mm") + ' a ' + self.configuration_panel_balance_products_panel_date_end.dateTime().toString("dd/MM/yyyy - hh:mm"))

        pdf.setFont("Ubuntu-Bold",8)
        pdf.drawString(float(20 * mm), float(297 * mm) - float(40 * mm), "Código")
        pdf.drawString(float(38 * mm), float(297 * mm) - float(40 * mm), "Nome")
        pdf.drawString(float(110 * mm), float(297 * mm) - float(40 * mm), "Descrição")
        pdf.drawString(float(175 * mm), float(297 * mm) - float(40 * mm), "Qtd.")
        pdf.drawRightString(float(190 * mm), float(297 * mm) - float(40 * mm), "Preço")

        line_distance = 50
        pdf.setFont("Ubuntu-Condensed",8)

        list_of_prices = []


        for item in list_of_products:
            product_profile = ConfigParser.RawConfigParser()
            product_profile.read(os.path.join(dirpathProducts, item.split(' - ')[0]))

            pdf.drawString(float(20 * mm), float(297 * mm) - float(line_distance * mm), item.split(' - ')[0])
            pdf.drawString(float(38 * mm), float(297 * mm) - float(line_distance * mm), product_profile.get('geral', 'nome'))
            pdf.drawString(float(110 * mm), float(297 * mm) - float(line_distance * mm), product_profile.get('geral', 'descricao'))
            pdf.drawString(float(175 * mm), float(297 * mm) - float(line_distance * mm), product_profile.get('geral', 'estoque'))
            pdf.drawRightString(float(190 * mm), float(297 * mm) - float(line_distance * mm), str("%.2f" % float(product_profile.get('geral', 'preco').replace(',', '.'))).replace('.', ','))

            list_of_prices.append(float(product_profile.get('geral', 'preco').replace(',', '.')) * int(product_profile.get('geral', 'estoque')))

            line_distance += 5
            if line_distance >= 277:
                pdf.showPage()
                pdf.setFont("Ubuntu-Condensed",8)
                line_distance = 20

        line_distance += 3

        pdf.setFont("Ubuntu-Bold",8)
        pdf.drawString(float(150 * mm), float(297 * mm) - float(line_distance * mm), 'TOTAL NA LOJINHA:')
        pdf.drawRightString(float(190 * mm), float(297 * mm) - float(line_distance * mm), str("%.2f" % sum(list_of_prices)).replace('.', ','))

        list_of_output_values = []

        for member in list_of_members:
            profile = ConfigParser.RawConfigParser()
            profile.read(os.path.join(dirpathNames, member.split(' - ')[0]))

            for product in profile.get('produtos', 'lista').split(','):
                if not product.split('-')[0][:12] == '' and int(product.split('-')[0][:12]) >= int(self.configuration_panel_balance_products_panel_date_start.dateTime().toString("yyyyMMddhhmm")) and int(product.split('-')[0][:12]) <= int(self.configuration_panel_balance_products_panel_date_end.dateTime().toString("yyyyMMddhhmm")):
                    list_of_output_values.append(float(float(product.split('-')[1].split('(')[1].split('-')[0]) * int(product.split('-')[2].split(')')[0])))

        line_distance += 5

        pdf.drawString(float(150 * mm), float(297 * mm) - float(line_distance * mm), 'TOTAL DE SAÍDA:')
        pdf.drawRightString(float(190 * mm), float(297 * mm) - float(line_distance * mm), str("%.2f" % sum(list_of_output_values)).replace('.', ','))

        pdf.showPage()
        pdf.save()

    def configuration_panel_balance_change_individual_option(self):
        if self.configuration_panel_balance_individual_panel_option_all.isChecked():
            self.configuration_panel_balance_individual_panel_option_one_name.setEnabled(False)
        else:
            self.configuration_panel_balance_individual_panel_option_one_name.setEnabled(True)
        self.configuration_panel_balance_button_individual_pdf.setEnabled(True)
        self.configuration_panel_balance_button_individual_csv.setEnabled(True)

    def configuration_panel_configuration_backup_make_package_selected(self):
        self.location_path = QtGui.QFileDialog.getSaveFileName(self, u"Salvar o pacote de backup", os.environ.get('HOME', None), unicode(str(u"Pacote ZIP (*.zip)"), 'utf-8')).toUtf8()

        self.generate_backup_package(self.location_path)

    def generate_backup_package(self, filename):
        z = zipfile.ZipFile(filename, 'w')
        for name_file in os.listdir(os.path.join(dirpathConfig, 'irmaos')):
            z.write(os.path.join(dirpathConfig, 'irmaos', name_file), os.path.join('irmaos', name_file))
        for product_file in os.listdir(os.path.join(dirpathConfig, 'produtos')):
            z.write(os.path.join(dirpathConfig, 'produtos', product_file), os.path.join('produtos', product_file))
        z.write(os.path.join(dirpathConfig, 'config.cfg'), os.path.join('config.cfg'))
        z.close()

    def configuration_panel_configuration_program_name_field1_change(self):
        mainConfigFile.set('geral', 'titulo-principal', self.configuration_panel_configuration_program_name_field1.text())
        self.top_panel_title.setText(unicode(mainConfigFile.get('geral', 'titulo-principal'), 'utf-8'))
        mainConfigFile.write(open(os.path.join(dirpathConfig, 'config.cfg'), 'wb'))

    def configuration_panel_configuration_program_name_field2_change(self):
        mainConfigFile.set('geral', 'subtitulo', self.configuration_panel_configuration_program_name_field2.text())
        self.top_panel_title_place.setText(unicode(mainConfigFile.get('geral', 'subtitulo'), 'utf-8'))
        mainConfigFile.write(open(os.path.join(dirpathConfig, 'config.cfg'), 'wb'))

    def configuration_panel_configuration_program_donate_number_change(self):
        profile = ConfigParser.RawConfigParser()
        profile.read(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_configuration_program_donate_number.value()))))
        if os.path.isfile(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_configuration_program_donate_number.value())))):
            self.configuration_panel_configuration_program_donate_number_warning.setShown(True)
            self.configuration_panel_configuration_program_donate_number_warning.setText(u"Esse é o número de " + unicode(profile.get('geral', 'nome'), 'utf-8') + '.')
        else:
            self.configuration_panel_configuration_program_donate_number_warning.setShown(False)
            new_profile = ConfigParser.RawConfigParser()
            new_profile.read(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo')))))
            new_profile.write(open(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(self.configuration_panel_configuration_program_donate_number.value()))), 'wb'))
            os.remove(os.path.join(dirpathNames, str(str("%0" + mainConfigFile.get('geral', 'membro-digitos') + "d") % int(mainConfigFile.get('geral', 'membro-donativo')))))

            mainConfigFile.set('geral', 'membro-donativo', str(self.configuration_panel_configuration_program_donate_number.value()))
            mainConfigFile.write(open(os.path.join(dirpathConfig, 'config.cfg'), 'wb'))

    def configuration_panel_configuration_program_members_digits_change(self):
        for name_file in os.listdir(os.path.join(dirpathConfig, 'irmaos')):
            shutil.move(os.path.join(dirpathConfig, 'irmaos', name_file), os.path.join(dirpathConfig, 'irmaos', str(str("%0" + str(int(self.configuration_panel_configuration_program_members_digits.value())) + "d") % int(name_file))))
        mainConfigFile.set('geral', 'membro-digitos', str(self.configuration_panel_configuration_program_members_digits.value()))
        mainConfigFile.write(open(os.path.join(dirpathConfig, 'config.cfg'), 'wb'))

    def configuration_panel_configuration_program_actual_password_change(self):
        if self.configuration_panel_configuration_program_actual_password.text() == mainConfigFile.get('geral', 'senha'):
            self.configuration_panel_configuration_program_new1_password_title.setEnabled(True)
            self.configuration_panel_configuration_program_new1_password.setEnabled(True)
            self.configuration_panel_configuration_program_new2_password_title.setEnabled(True)
            self.configuration_panel_configuration_program_new2_password.setEnabled(True)
        else:
            self.configuration_panel_configuration_program_new1_password_title.setEnabled(False)
            self.configuration_panel_configuration_program_new1_password.setEnabled(False)
            self.configuration_panel_configuration_program_new2_password_title.setEnabled(False)
            self.configuration_panel_configuration_program_new2_password.setEnabled(False)

    def configuration_panel_configuration_program_new_password_change(self):
        if self.configuration_panel_configuration_program_new2_password.text() == self.configuration_panel_configuration_program_new1_password.text() and not self.configuration_panel_configuration_program_new2_password.text() == '':
            mainConfigFile.set('geral', 'senha', self.configuration_panel_configuration_program_new2_password.text())
            mainConfigFile.write(open(os.path.join(dirpathConfig, 'config.cfg'), 'wb'))
            self.configuration_panel_configuration_program_actual_password.setText('')
            self.configuration_panel_configuration_program_new1_password_title.setEnabled(False)
            self.configuration_panel_configuration_program_new1_password.setEnabled(False)
            self.configuration_panel_configuration_program_new1_password.setText('')
            self.configuration_panel_configuration_program_new2_password_title.setEnabled(False)
            self.configuration_panel_configuration_program_new2_password.setEnabled(False)
            self.configuration_panel_configuration_program_new2_password.setText('')

    def configuration_panel_new_products_code_change(self):
        if len(str(self.configuration_panel_new_products_code.text())) >= 1 and len(str(self.configuration_panel_new_products_code.text())) < 8:
            original_number = self.configuration_panel_new_products_code.text()
            self.configuration_panel_new_products_code.setText(str("%08d" % int(original_number)))
        if len(str(self.configuration_panel_new_products_code.text())) <= 13 and len(str(self.configuration_panel_new_products_code.text())) >= 8:
            if os.path.isfile(os.path.join(dirpathProducts, str(self.configuration_panel_new_products_code.text()))):
                product_profile = ConfigParser.RawConfigParser()
                product_profile.read(os.path.join(dirpathProducts, str(self.configuration_panel_new_products_code.text())))

                self.configuration_panel_new_products_code_title2.setShown(True)
                self.configuration_panel_new_products_code_title3.setShown(True)
                self.configuration_panel_new_products_number_title.setShown(True)
                self.configuration_panel_new_products_number.setShown(True)
                self.configuration_panel_new_products_price_title.setShown(True)
                self.configuration_panel_new_products_price.setShown(True)
                self.configuration_panel_new_products_cancel.setShown(True)
                self.configuration_panel_new_products_confirm.setShown(True)
                self.configuration_panel_new_products_code.setEnabled(False)

                self.configuration_panel_new_products_code_title2.setText('<strong>' + unicode(product_profile.get('geral', 'nome'), 'utf-8') + '</strong><br></br><small>' + unicode(product_profile.get('geral', 'descricao'), 'utf-8') + ' - ' + product_profile.get('geral', 'estoque') + ' un. - R$ ' + product_profile.get('geral', 'preco').replace('.', ',') + '</small>')
                if int(product_profile.get('geral', 'estoque')) == 1:
                    self.configuration_panel_new_products_code_title3.setText(u'Existe <strong>1 item</strong> no estoque, no valor de <strong>R$ ' + product_profile.get('geral', 'preco').replace('.', ',') + '</strong>.')
                else:
                    self.configuration_panel_new_products_code_title3.setText(u'Existem <strong>' + product_profile.get('geral', 'estoque') + ' itens</strong> no estoque no valor de <strong>R$ ' + product_profile.get('geral', 'preco').replace('.', ',') + '</strong> cada, totalizando <strong>R$ ' + str("%.2f" % float(float(product_profile.get('geral', 'preco').replace(',', '.')) * int(product_profile.get('geral', 'estoque') ))).replace('.', ',') + '</strong>.')
            else:
                self.configuration_panel_new_products_code.setText('')

    def configuration_panel_new_products_numbers_change(self):
        product_profile = ConfigParser.RawConfigParser()
        product_profile.read(os.path.join(dirpathProducts, str(self.configuration_panel_new_products_code.text())))

        if not self.configuration_panel_new_products_price.text() == ',':
            self.configuration_panel_new_products_code_title3.setText(u'Haverá <strong>' + str(int(product_profile.get('geral', 'estoque')) + int(self.configuration_panel_new_products_number.text()))  + '</strong> itens no estoque no valor de <strong>R$ ' + str("%0.2f" % float(float(float(float(product_profile.get('geral', 'preco').replace(',', '.')) * int(product_profile.get('geral', 'estoque') )) + float(self.configuration_panel_new_products_price.text().replace(',', '.'))) / int(int(product_profile.get('geral', 'estoque')) + int(self.configuration_panel_new_products_number.text())))).replace('.', ',') + '</strong> cada, totalizando <strong>R$ ' + str("%.2f" % float(float(float(float(product_profile.get('geral', 'preco').replace(',', '.')) * int(product_profile.get('geral', 'estoque') )) + float(self.configuration_panel_new_products_price.text().replace(',', '.'))))).replace('.', ',') + '</strong>.')

    def configuration_panel_new_products_confirm_click(self):
        product_profile = ConfigParser.RawConfigParser()
        product_profile.read(os.path.join(dirpathProducts, str(self.configuration_panel_new_products_code.text())))

        if not self.configuration_panel_new_products_price.text() == ',':
            actual_stock = int(product_profile.get('geral', 'estoque'))
            product_profile.set('geral', 'estoque', str(actual_stock + int(self.configuration_panel_new_products_number.text())))
            actual_price = product_profile.get('geral', 'preco')
            product_profile.set('geral', 'preco', str("%0.2f" % float(float(float(float(actual_price.replace(',', '.')) * actual_stock) + float(self.configuration_panel_new_products_price.text().replace(',', '.'))) / int(actual_stock + int(self.configuration_panel_new_products_number.text())))).replace('.', ','))
            product_profile.write(open(os.path.join(dirpathProducts, str(self.configuration_panel_new_products_code.text())), 'wb'))

        self.configuration_panel_new_products_cancel_click()

    def configuration_panel_new_products_cancel_click(self):
        self.configuration_panel_new_products_code.setText('')
        self.configuration_panel_new_products_code_title2.setShown(False)
        self.configuration_panel_new_products_code_title3.setShown(False)
        self.configuration_panel_new_products_number_title.setShown(False)
        self.configuration_panel_new_products_number.setShown(False)
        self.configuration_panel_new_products_number.setValue(1)
        self.configuration_panel_new_products_price_title.setShown(False)
        self.configuration_panel_new_products_price.setShown(False)
        self.configuration_panel_new_products_price.setText('')
        self.configuration_panel_new_products_cancel.setShown(False)
        self.configuration_panel_new_products_confirm.setShown(False)
        self.configuration_panel_new_products_code.setEnabled(True)


app = QtGui.QApplication(sys.argv)
app.main = mainWindow()
app.main.show()

sys.exit(app.exec_())

